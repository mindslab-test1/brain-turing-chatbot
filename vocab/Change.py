import random


class Change:
    def __init__(self, p_yamin=.0, p_phocjz=.0, p_hanyeong=.0, p_dochi=.0, p_insdel=.0):
        self.p_list = [p_yamin, p_phocjz, p_hanyeong, p_dochi, p_insdel]

        self.cjz = [list("ㄱㄲㄴㄷㄸㄹㅁㅂㅃㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎ"),
                    list("ㅏㅐㅑㅒㅓㅔㅕㅖㅗㅘㅙㅚㅛㅜㅝㅞㅟㅠㅡㅢㅣ"),
                    [chr(12644)] + list("ㄱㄲㄳㄴㄵㄶㄷㄹㄺㄻㄼㄽㄾㄿㅀㅁㅂㅄㅅㅆㅇㅈㅊㅋㅌㅍㅎ")]

        wsh = 'ㄱㄲㄳㄴㄵㄶㄷㄸㄹㄺㄻㄼㄽㄾㄿㅀㅁㅂㅃㅄㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎㅏㅐㅑㅒㅓㅔㅕㅖㅗㅘㅙㅚㅛㅜㅝㅞㅟㅠㅡㅢㅣ'
        zhh = 'ᄀᄁᆪᄂᆬᆭᄃᄄᄅᆰᆱᆲᆳᆴᆵᄚᄆᄇᄈᄡᄉᄊᄋᄌᄍᄎᄏᄐᄑ하ᅢᅣᅤᅥᅦᅧᅨᅩᅪᅫᅬᅭᅮᅯᅰᅱᅲᅳᅴᅵ'

        self.johapsiba = {k: v for k, v in zip(zhh, wsh)}

        self.bunhe = {
            'ㄳ': 'ㄱㅅ',
            'ㄵ': 'ㄴㅈ',
            'ㄶ': 'ㄴㅎ',
            'ㄺ': 'ㄹㄱ',
            'ㄻ': 'ㄹㅁ',
            'ㄼ': 'ㄹㅂ',
            'ㄽ': 'ㄹㅅ',
            'ㄾ': 'ㄹㅌ',
            'ㄿ': 'ㄹㅍ',
            'ㅀ': 'ㄹㅎ',
            'ㅄ': 'ㅂㅅ',
            'ㅘ': 'ㅗㅏ',
            'ㅙ': 'ㅗㅐ',
            'ㅚ': 'ㅗㅣ',
            'ㅝ': 'ㅜㅓ',
            'ㅞ': 'ㅜㅔ',
            'ㅟ': 'ㅜㅣ',
            'ㅢ': 'ㅡㅣ',
            chr(12644): ''
        }

        self.hebun = {k: v for v, k in self.bunhe.items()}

        self.hanyeong = {k: v for k, v in zip("QqWwEeRrTtyuiOoPpasdfghjklzxcvbnm", "ㅃㅂㅉㅈㄸㄷㄲㄱㅆㅅㅛㅕㅑㅒㅐㅖㅔㅁㄴㅇㄹㅎㅗㅓㅏㅣㅋㅌㅊㅍㅠㅜㅡ")}
        self.hanyeong.update({v: k for k, v in self.hanyeong.items()})
        
        self.yamin = {
            '대': '머',
            '귀': '커',
            '파': '과',
            '피': '끠',
            '비': '네',
            '며': '띠',
            '거': '지',
            '겨': '저',
            '교': '꼬',
            '유': '윾',
            '우': '윽',
            '왕': '앟',
            '왱': '앻',
            '욍': '잏',
            '왓': '앛',
            '왯': '앷',
            '욋': '잋',
            'ㅇ': 'o',
            'ㄱ': '7',
            'ㄹ': '2',
            '디': 'ㅁ',
            '구': 'ㅋ',
            '너': 'ㅂ',
            '빅': '븨',
            '근': 'ㄹ',
            '긘': '리'
        }
        self.yamin.update({v: k for k, v in self.yamin.items()})

        self.phocjz = [[list('ㄲㅋ'),
                        list('ㄱㅋ'),
                        list('ㄴ'),
                        list('ㄸㅌ'),
                        list('ㄷㅌ'),
                        list('ㄹ'),
                        list('ㅁ'),
                        list('ㅃㅍ'),
                        list('ㅂㅍ'),
                        list('ㅆ'),
                        list('ㅅ'),
                        list('ㅇ'),
                        list('ㅉㅊ'),
                        list('ㅈㅊ'),
                        list('ㅈㅉ'),
                        list('ㄱㄲ'),
                        list('ㄷㄸ'),
                        list('ㅂㅃ'),
                        list('ㅎ')
                        ],
                       [list('ㅑㅘ'),
                        list('ㅒㅔㅖㅙㅚㅞ'),
                        list('ㅏㅘ'),
                        list('ㅐㅔㅖㅙㅚㅞ'),
                        list('ㅕㅗㅛㅝ'),
                        list('ㅐㅒㅖㅙㅚㅞ'),
                        list('ㅓㅗㅛㅝ'),
                        list('ㅐㅒㅔㅙㅚㅞ'),
                        list('ㅓㅕㅛㅝ'),
                        list('ㅏㅑ'),
                        list('ㅐㅒㅔㅖㅚㅞ'),
                        list('ㅐㅒㅔㅖㅙㅞ'),
                        list('ㅓㅕㅗㅝ'),
                        list('ㅡㅠ'),
                        list('ㅓㅕㅗㅛ'),
                        list('ㅐㅒㅔㅖㅙㅚ'),
                        list('ㅢㅣ'),
                        list('ㅜㅡ'),
                        list('ㅜㅠ'),
                        list('ㅟㅣ'),
                        list('ㅟㅢ')
                        ],
                       [list('ㅎ'),
                        list('ㄲㄳㄺㅋ'),
                        list('ㄱㄳㄺㅋ'),
                        list('ㄱㄲㄺㅋ'),
                        list('ㄵㄶ'),
                        list('ㄴㄶ'),
                        list('ㄴㄵ'),
                        list('ㅅㅆㅈㅊㅌ'),
                        list('ㄽㄾㅀ'),
                        list('ㄱㄲㄳㅋ'),
                        list('ㅁ'),
                        list('ㄿㅂㅄㅍ'),
                        list('ㄹㄾㅀ'),
                        list('ㄹㄽㅀ'),
                        list('ㄼㅂㅄㅍ'),
                        list('ㄹㄽㄾ'),
                        list('ㄻ'),
                        list('ㄼㄿㅄㅍ'),
                        list('ㄼㄿㅂㅍ'),
                        list('ㄷㅆㅈㅊㅌ'),
                        list('ㄷㅅㅈㅊㅌ'),
                        list('ㅇ'),
                        list('ㄷㅅㅆㅊㅌ'),
                        list('ㄷㅅㅆㅈㅌ'),
                        list('ㄱㄲㄳㄺ'),
                        list('ㄷㅅㅆㅈㅊ'),
                        list('ㄼㄿㅂㅄ'),
                        list(chr(12644))
                        ]
                       ]

    def sen2seq(self, sen, destroy=False, mix=False, turb=False):
        p_list = [.0] * 5
        if destroy:
            if mix:
                p_list = self.p_list
            else:
                only_p = random.randrange(5)
                p_list[only_p] = self.p_list[only_p]
            if turb:
                p_list = [i * random.random() for i in p_list]

        seq = ""
        for cha in sen:
            seq += self.cha2seq(cha, p_list[:3])

        if destroy:
            seq = self.destroy(seq, p_list[3:])
        return seq

    def cha2seq(self, cha, p_list):
        if random.random() < p_list[0]:
            cha = self.yamin.get(cha, cha)
        ordcha = ord(cha)
        if 44032 <= ordcha <= 55203:
            ordcha -= 44032
            cha3 = [ordcha // (21 * 28),
                    ordcha % (21 * 28) // 28,
                    ordcha % (21 * 28) % 28]

            cha_seq = []
            for i, c in enumerate(cha3):
                if random.random() < p_list[1]:
                    g = random.choice(self.phocjz[i][c])
                else:
                    g = self.cjz[i][c]            
                cha_seq.append(g)

            cha_g = ""
            for i in cha_seq:
                if random.random() < p_list[2]:
                    i = self.hanyeong.get(i, i)
                cha_g += self.bunhe.get(i, i)

        else:
            if cha == '\n':
                cha = '<n>'

            cha_g = cha

        c = ""
        for i in cha_g:
            c += self.johapsiba.get(i, i)

        return c

    def destroy(self, seq, p_list):
        pp = p_list[0]
        for i in range(len(seq)):
            if i in [0, len(seq) - 1]:
                continue
            r = random.random()
            if r < pp:
                seq = seq[:i] + seq[i+1] + seq[i] + seq[i+2:]
                pp /= 10
            elif r > 1 - pp:
                seq = seq[:i-1] + seq[i] + seq[i-1] + seq[i+1:]
                pp /= 10
            else:
                pp = p_list[0]

        pp = p_list[1]
        i = 0
        while i < len(seq):
            r = random.random()
            if r < pp:
                if r < pp / 10:
                    seq = seq[:i] + random.choice([chr(i) for i in range(32, 127)]) + seq[i:]
                else:
                    seq = seq[:i] + seq[i] + seq[i:]
                i += 2
                pp /= 10
            elif r > 1 - pp / 2:
                seq = seq[:i] + seq[i + 1:]
                pp /= 10
            else:
                i += 1
                pp = p_list[1]

        return seq

    def seq2cha(self, seq):
        cjz = [self.cjz[i].index(seq[i]) for i in range(3)]
        cha = chr(cjz[0] * (21 * 28) + cjz[1] * 28 + cjz[2] + 44032)
        return cha

    def seq2sen(self, seq):
        seq = [self.johapsiba.get(i, i) for i in seq]

        seq.append('')
        sen = ""
        que = []
        quen = ""
        while len(seq) > 0 or len(que) > 0:
            try:
                if seq[0] in self.cjz[0]:
                    quen += '0'
                elif seq[0] in self.cjz[1]:
                    quen += '1'
                else:
                    quen += '2'

                que.append(seq[0])
                seq = seq[1:]

            except:
                pass

            banbok = True
            n_bb = 0
            while banbok:
                n_bb += 1
                if n_bb > 20:
                    print('seq', seq)
                    print('que', que)
                    print('qun', quen)
                    print('sen', sen)
                    assert False
                if quen == "11" or quen == "000" or quen == "002":
                    abc = que[0] + que[1]
                    abc = self.hebun.get(abc, abc)
                    sen += abc[0]
                    que = que[3 - len(abc):]
                    quen = quen[3 - len(abc):]
                elif quen == "001" or quen == "10" or quen == "2" or quen == "02" or quen == "12":
                    sen += que[0]
                    que = que[1:]
                    quen = quen[1:]
                elif quen == "011":
                    abc = que[1] + que[2]
                    if abc not in self.hebun:
                        cha3 = [que[0], que[1], chr(12644)]
                        sen += self.seq2cha(cha3)
                        que = que[2:]
                        quen = quen[2:]
                elif quen == "0111" or quen == "01101":
                    abc = que[1] + que[2]
                    cha3 = [que[0], self.hebun.get(abc), chr(12644)]
                    sen += self.seq2cha(cha3)
                    que = que[3:]
                    quen = quen[3:]
                elif quen == "0110" or quen == "0112":
                    abc = que[1] + que[2]
                    if que[3] not in self.cjz[2]:
                        cha3 = [que[0], self.hebun.get(abc), chr(12644)]
                        sen += self.seq2cha(cha3)
                        que = que[3:]
                        quen = quen[3:]
                elif quen == "01100" or quen == "01102":
                    abc = que[1] + que[2]
                    bcd = que[3] + que[4]
                    if bcd not in self.hebun:
                        cha3 = [que[0], self.hebun.get(abc), que[3]]
                        sen += self.seq2cha(cha3)
                        que = que[4:]
                        quen = quen[4:]
                elif quen == "011000" or quen == "011002":
                    abc = que[1] + que[2]
                    bcd = que[3] + que[4]
                    cha3 = [que[0], self.hebun.get(abc), self.hebun.get(bcd)]
                    sen += self.seq2cha(cha3)
                    que = que[5:]
                    quen = quen[5:]
                elif quen == "011001":
                    abc = que[1] + que[2]
                    cha3 = [que[0], self.hebun.get(abc), que[3]]
                    sen += self.seq2cha(cha3)
                    que = que[4:]
                    quen = quen[4:]
                elif quen == "010":
                    if que[2] not in self.cjz[2]:
                        cha3 = [que[0], que[1], chr(12644)]
                        sen += self.seq2cha(cha3)
                        que = que[2:]
                        quen = quen[2:]
                elif quen == "0100" or quen == "0102":
                    bcd = que[2] + que[3]
                    if bcd not in self.hebun:
                        cha3 = [que[0], que[1], que[2]]
                        sen += self.seq2cha(cha3)
                        que = que[3:]
                        quen = quen[3:]
                elif quen == "0101" or quen == "012":
                    cha3 = [que[0], que[1], chr(12644)]
                    sen += self.seq2cha(cha3)
                    que = que[2:]
                    quen = quen[2:]
                elif quen == "01000" or quen == "01002":
                    bcd = que[2] + que[3]
                    cha3 = [que[0], que[1], self.hebun.get(bcd)]
                    sen += self.seq2cha(cha3)
                    que = que[4:]
                    quen = quen[4:]
                elif quen == "01001":
                    cha3 = [que[0], que[1], que[2]]
                    sen += self.seq2cha(cha3)
                    que = que[3:]
                    quen = quen[3:]

                if not quen.endswith('2'):
                    banbok = False

        return sen


if __name__ == "__main__":

    '''
    S2S Test zone
    '''

    itog = ['<unk>', '<mask>', '<eot>', '<pad>', '<sos>', '<eos>'] + \
           [chr(i) for i in range(32, 127)] + \
           [chr(i) for i in range(12593, 12645)]

    change = Change(.1,.1,.1,.1,.1)

    text = "나라말이 중국과 달라 한문 한자와 서로 통하지 아니하므로 이런 까닭으로 어리석은 백성들이 말하고자 하는 바가 있어도 끝내 제 뜻을 펴지 못하는 사람이 많다. 내가 이를 불쌍히 여겨 새로 스물 여덟 글자를 만드니 사람마다 하여금 쉽게 익혀 날마다 씀에 편하게 하고자 할 따름이다. 동해물과 백두산이 마르고 닳도록 하느님이 보우하사 우리나라 만세 무궁화 삼천리 화려강산 대한사람 대한으로 길이 보전하세 남산 위에 저 소나무 철갑을 두른 듯 바람 서리 불변함은 우리 기상일세 가을 하늘 공활한데 높고 구름 없이 밝은 달은 우리 가슴 일편단심일세 이 기상과 이 맘으로 충성을 다하여 괴로우나 즐거우나 나라 사랑하세."
    # text = "야얗이 나븐\n 놂아!"

    print(change.seq2sen(change.sen2seq(text, destroy=True, mix=True, turb=True)))