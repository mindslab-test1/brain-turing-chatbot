import sentencepiece as spm
from konlpy.tag import Mecab

from .Change import Change


class MCSTokenizer:
    def __init__(self, tagger=None, change=None, sp=None):
        self.m = tagger
        self.c = change
        self.s = sp

    def preprocess(self, text, destroy=False, mix=False, turb=False):
        text = text.replace(' ', chr(9601)).replace('\n', chr(9602))
        if self.m is not None:
            try:
                text = "<ws>".join(self.m.morphs(text))
            except:
                with open('mecab_morphs_error.txt', 'w', encoding='utf-8') as f:
                    f.write(text + '\n')
        text = text.replace(chr(9602), '\n')
        text = text.replace(chr(9601), ' ').replace('<ws> <ws>', ' ')
        if self.c is not None:
            text = self.c.sen2seq(text, destroy=destroy, mix=mix, turb=turb)
        return text

    def EncodeAsPieces(self, text, destroy=False, mix=False, turb=False):
        text = self.preprocess(text, destroy=destroy, mix=mix, turb=turb)
        if self.s is not None:
            text = self.s.EncodeAsPieces(text)
        return [t for t in text if t != '<ws>']

    def EncodeAsIds(self, text, destroy=False, mix=False, turb=False):
        text = self.preprocess(text, destroy=destroy, mix=mix, turb=turb)
        if self.s is not None:
            text = self.s.EncodeAsIds(text)
        return [t for t in text if t != 5]

    def Decode(self, text, geheng=False):
        if self.s is not None:
            text = self.s.Decode(text)
        '''text = text.replace('<ws>', chr(9601))
        if self.c is not None:
            text = self.c.seq2sen(self.c.sen2seq(text))
        text = text.replace(' ', '').replace(chr(9601), ' ')'''
        if geheng:
            text = text.replace('<n>', '\n')
        return text


if __name__ == "__main__":
    '''
    if change = None, Mecab + SentencePiece,
    if change = Change(), Mecab + Change(grapheme level) + SentencePiece
    '''
    sp = spm.SentencePieceProcessor(model_file='vocab/mecab_change_50K_uni.model')
    grpt = MCSTokenizer(tagger=Mecab(), change=Change(), sp=sp)

    text = "안녕하세요, 사랑하는 사람과 함께하고 계신가요?\n네, 저는 지금 행복합니다."

    seq = grpt.EncodeAsPieces(text)
    print(seq)
    onemun = grpt.Decode(seq)
    print(onemun)