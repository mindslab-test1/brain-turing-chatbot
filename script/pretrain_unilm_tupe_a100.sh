#!/bin/bash

CUDA_VISIBLE_DEVICES=0,1,2,3 python Main.py \
    --enc_seq_len                                       1024 \
    --dec_seq_len                                       -1 \
    --embed_size                                        128 \
    --hidden_size                                       1024 \
    --enc_n_layers                                      16 \
    --dropout                                           0.1 \
    --multi_heads                                       16 \
    --enc_shra_sharing                                  \
    --max_token_len_per_segment                         1028 \
    --max_segment_length                                256 \
    --segment_embedding                                 fixed_embedding_matrix \
\
    --attention_scale_factor                            2.0 \
    --absolute_position_encoding                        untied_absolute_learnable \
    --global_to_internal_offset                         1 \
    --global_to_external_global_offset                  2 \
    --normal_to_internal_global_offset                  3 \
    --normal_to_external_global_offset                  4 \
    --use_t5_relative_position_encoding                 \
    --t5_relative_position_encoding_num_buckets         32 \
    --t5_relative_position_encoding_max_distance        128 \
\
    --learning_rate                                     3.0e-04 \
    --adam_beta_1                                       0.9 \
    --adam_beta_2                                       0.999 \
    --adam_weight_decay                                 0.0 \
    --schedule_scheme                                   linear \
    --warmup_steps                                      22000 \
\
    --checkpoint_file_name                              unilm \
    --sentencepiece_model_path                          /root/UniLM/vocab/mecab_change_50K_bpe_yong.model \
    --save_top_k                                        1 \
\
    --gpus                                              -1  \
    --amp_backend                                       native \
    --amp_level                                         O2 \
    --precision                                         32 \
    --accelerator                                       dp \
    --log_every_n_steps                                 1 \
    --progress_bar_refresh_rate                         1 \
    --val_check_interval                                0.1 \
    --accumulate_grad_batches                           1 \
    --default_root_dir                                  checkpoint/wo_bigbunny \
    --gradient_clip_val                                 1.0 \
    --max_steps                                         770000 \
\
    --mask_ratio                                        0.15 \
    --max_seq_len                                       1024 \
    --seed                                              34 \
    --corpusdir                                         /root/UniLM/corpus/bpe_corpus/ \
    --batch_size                                        100 \
    --num_workers                                       10 \
    --pretrained_path                                   checkpoint/all_in_bpe_wo_weight_decay/lightning_logs/version_4/checkpoints/unilm_top_k-epoch\=0011-step\=242114-val_loss\=2.65400.ckpt