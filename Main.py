import argparse, torch, os
import sentencepiece as spm
from konlpy.tag import Mecab
# from omegaconf import OmegaConf
from lm.UniLM import UniLM
import pytorch_lightning as pl
from vocab.MCSTokenizer import MCSTokenizer
from vocab.Change import Change
from pytorch_lightning.callbacks import (
    ModelCheckpoint,
    EarlyStopping,
    LearningRateMonitor,
)
from Dataset import LitDataModule
from pytorch_lightning import LightningModule


class SaveCheckpointEveryNSteps(pl.Callback):
    def __init__(self, frequency, file_name):
        self.frequency = frequency
        self.file_name = file_name
    
    def on_batch_end(self, trainer, pl_module: LightningModule) -> None:
        step = trainer.global_step
        if step != 0 and step % self.frequency == 0:
            file_name = f'{self.file_name}-step={step}.ckpt'
            checkpoint_path = os.path.join(trainer.checkpoint_callback.dirpath, file_name)
            trainer.save_checkpoint(checkpoint_path)


# TODO: Move prediction printing code to this callback.
class LogPredictions(pl.Callback):
    def __init__(self):
        pass
    

def main(args):

    pl.utilities.seed.seed_everything(seed=args.seed)

    # Setup the model
    # TODO: It might be a good idea to use argparse
    # with open(args.model_config_path, 'r') as f:
    #     model_config = OmegaConf.load(f)

    sp_processor = spm.SentencePieceProcessor(model_file=args.sentencepiece_model_path)
    # TODO: Ask The Dragon what change is for
    mcs_tokenizer = MCSTokenizer(tagger=Mecab(), change=Change(), sp=sp_processor)
    model = UniLM(args, mcs_tokenizer)

    if args.pretrained_path is not None:
        print('Loading pretrained weights...')
        pretrained_checkpoint = torch.load(args.pretrained_path, map_location=torch.device('cpu'))
        # YongBERT weights are stored in model_state_dict
        if 'model_state_dict' in pretrained_checkpoint:
            weight_load_result = model.load_state_dict(pretrained_checkpoint['model_state_dict'], strict=False)
        # Lightning weights are stored in state_dict
        else:
            weight_load_result = model.load_state_dict(pretrained_checkpoint['state_dict'], strict=False)
        print('Weight load results:', weight_load_result)

    # Setup the datamodule
    # TODO: It might be a good idea to use argparse
    # with open(args.datamodule_config_path, 'r') as f:
    #     datamodule_config = OmegaConf.load(f)

    datamodule = LitDataModule(args, mcs_tokenizer, batch_size=args.batch_size)

    # Setup the callbacks
    callbacks = []
    checkpoint_top_k_callback = ModelCheckpoint(
        monitor='val_loss',
        filename=args.checkpoint_file_name + '_top_k' + '-{epoch:04d}-{step}-{val_loss:.5f}',
        save_top_k=args.save_top_k,
        mode='min',
    )
    callbacks.append(checkpoint_top_k_callback)

    if args.save_backup_checkpoints:
        # There will be duplicate checkpoints.
        checkpoint_every_n_callback = ModelCheckpoint(
            filename=args.checkpoint_file_name + '_backup' + '-{epoch:04d}-{step}-{val_loss:.5f}',
            save_top_k=-1,
        )
        callbacks.append(checkpoint_every_n_callback)
    
    if args.save_checkpoint_every_n_steps is not None:
        callbacks.append(SaveCheckpointEveryNSteps(
            frequency=args.save_checkpoint_every_n_steps,
            file_name=args.checkpoint_file_name,
        )) 

    learning_rate_monitor_callback = LearningRateMonitor(
        logging_interval=None,
        log_momentum=False,
    )
    callbacks.append(learning_rate_monitor_callback)

    if args.early_stopping_patience is not None:
        early_stopping_callback = EarlyStopping(
            monitor='val_loss',
            min_delta=args.early_stopping_min_delta,
            patience=args.early_stopping_patience,
            verbose=True,
            mode='min',
        )
        callbacks.append(early_stopping_callback)


    trainer = pl.Trainer.from_argparse_args(
        args,
        callbacks=callbacks,
        profiler='simple'
    )

    # Run trainer
    # trainer.tune(model=model, datamodule=datamodule)
    trainer.fit(model=model, datamodule=datamodule)


def parse_args():
    parser = argparse.ArgumentParser()

    # Trainer related args
    parser = pl.Trainer.add_argparse_args(parser)
    parser.add_argument('--checkpoint_file_name', type=str, default='UniLM')
    parser.add_argument('--sentencepiece_model_path', type=str)
    parser.add_argument('--early_stopping_patience', type=int)
    parser.add_argument('--early_stopping_min_delta', type=float)
    parser.add_argument('--save_top_k', type=int, default=1)
    parser.add_argument('--save_checkpoint_every_n_epoch', type=int)
    parser.add_argument('--save_checkpoint_every_n_steps', type=int)
    parser.add_argument('--save_backup_checkpoints', action='store_true')

    # YougBERT Path
    parser.add_argument('--pretrained_path', type=str)

    # Which one would be a better choice, argparse or config?
    # parser.add_argument('--pretrained_config_path', type=str, default='yong_bert/yong_bert_config.yaml') 
    # parser.add_argument('--datamodule_config_path', type=str)

    # Model related args
    parser.add_argument('--enc_seq_len', type=int, default=2048)
    parser.add_argument('--dec_seq_len', type=int, default=-1)
    parser.add_argument('--embed_size', type=int, default=128)
    parser.add_argument('--hidden_size', type=int, default=1024)
    parser.add_argument('--enc_n_layers', type=int, default=16)
    parser.add_argument('--dropout', type=float, default=0.1)
    parser.add_argument('--inference', action='store_true')
    parser.add_argument('--multi_heads', type=int, default=16)
    parser.add_argument('--enc_shra_sharing', action='store_true', help="Attention layer 의 parameter 를 sharing 할지말지")
    parser.add_argument('--max_token_len_per_segment', type=int)
    parser.add_argument('--max_segment_length', type=int)
    parser.add_argument('--segment_embedding', default='fixed_embedding_matrix', choices=['fixed_embedding_matrix', 'linear'])

    # Debugging purposes. 
    # DO NOT USE. Leave them be.
    parser.add_argument('--full_bidirectional', action='store_true')
    parser.add_argument('--full_absolute_position_encoding', action='store_true')
    parser.add_argument('--decouple_mlm_head', action='store_true')

    # TUPE related args.
    parser.add_argument('--attention_scale_factor', type=float)
    parser.add_argument('--absolute_position_encoding', choices=['tied_absolute_matrix', 'untied_absolute_learnable', 'tied_absolute_linear', None])
    parser.add_argument('--use_t5_relative_position_encoding', action='store_true')
    parser.add_argument('--t5_relative_position_encoding_num_buckets', type=int)
    parser.add_argument('--t5_relative_position_encoding_max_distance', type=int)
    parser.add_argument('--global_to_internal_offset', type=int)
    parser.add_argument('--global_to_external_global_offset', type=int)
    parser.add_argument('--normal_to_internal_global_offset', type=int)
    parser.add_argument('--normal_to_external_global_offset', type=int)

    # Learning rate related args.
    parser.add_argument('--learning_rate', type=float)
    parser.add_argument('--adam_beta_1', type=float)
    parser.add_argument('--adam_beta_2', type=float)
    parser.add_argument('--adam_weight_decay', type=float)
    parser.add_argument('--warmup_steps', type=int)
    parser.add_argument('--num_cycles', type=int, default=2)
    parser.add_argument(
        '--schedule_scheme', 
        type=str, 
        default='linear', 
        choices=['linear', 'hard_cosine', 'none'],
        help="learning rate scheduler scheme : ['linear', 'hard_cosine', 'none']"
    )
    
    # Datamodule configs
    parser.add_argument('--mask_ratio', default=0.15, type=float)
    parser.add_argument('--max_seq_len', type=int)
    parser.add_argument('--seed', default=34, type=int)
    parser.add_argument('--corpusdir')
    parser.add_argument('--num_workers', default=1, type=int)

    # Batch size
    parser.add_argument('--batch_size', type=int, default=2)

    # For debugging
    parser.add_argument('--debug', action='store_true')

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    main(args)