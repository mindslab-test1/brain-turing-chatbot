#!/usr/bin/env python3
# All rights reserved.
#
# This source code is licensed under the license found in the LICENSE file in
# the root directory of this source tree. An additional grant of patent rights
# can be found in the PATENTS file in the same directory.
"""
Data pre-processing: build vocabularies and binarize training data.
"""
import argparse, os, logging, shutil, json, math
from omegaconf import OmegaConf
from pathlib import Path
from collections import Counter
from itertools import zip_longest
import pickle
from multiprocessing import Process, current_process

import torch

from Korpora import Korpora
from data.blocked_dataset import BlockedRawPlaintextDataset, BlockedGTDataset, BlockedPLATODataset

from multiprocessing import Pool, Manager, Process
from kss import split_sentences

from vocab.MCSTokenizer import MCSTokenizer
import sentencepiece as spm
from konlpy.tag import Mecab
from vocab.Change import Change

logger = logging.getLogger(__name__)

# a config file containing constants about paths & file names
CONST_CFG = 'cfg/const.yml'
const = OmegaConf.load(CONST_CFG)


# load corpus file
def safe_readline(f):
    pos = f.tell()
    while True:
        try:
            return f.readline()
        except UnicodeDecodeError:
            pos -= 1
            f.seek(pos)


def read_Korpora_corpus(title):
    """
    input: f (str for Path) for dir
    korean_petitions : title\ncontent \n\n
    output: doc 단위로 저장된 list
    """
    corpora = Korpora.load(title)
    corpora = corpora.get_all_texts()

    return corpora


def read_text_corpus(f, file_path=None):
    """
    input : f(str) : file path or file_dir path
    output: doc 단위로 저장된 list
    """
    file_path = Path('/data/engines/' + f)
    files = file_path.glob('*.txt') if file_path.is_dir() else [str(file_path)]
    corpora = []
    for corpus_file in files:
        with open(corpus_file, 'r', encoding='utf-8') as f:
            _corpora = f.read().split('\n\n')
        for c in _corpora[1:]:
            if c.startswith('주제'):
                corpora.append('\n'.join(c.split('\n')[1:]))
            else:
                corpora.append(c)
    return corpora


def read_gt_corpus(f, file_path=None):
    """
    input : f(str): file path or file_dir path
    output: dict 단위로 저장된 list
    """
    file_path = Path('/data/engines/' + f)
    files = list(file_path.glob('**/*.json')) if file_path.is_dir() else [file_path]
    corpora = []
    for corpus_file in files:
        total_utter_num = 0
        with open(corpus_file, 'r', encoding='utf-8') as j_file:
            _corpora = json.load(j_file)['data']
        print(f'total dialog number in {str(corpus_file.stem)} : [{len(_corpora)}]')
        for c in _corpora:
            dialog = c['dialog']
            corpora.append(dialog)
            total_utter_num += len(dialog)
        print(f'total utterance number in {str(corpus_file.stem)} : [{total_utter_num}]')
    print()
    return corpora


CORPUS_DIR = {
    'korwiki': ['lm/wiki_sent_data.txt', read_text_corpus],
    'namu': ['namuwikitext', read_Korpora_corpus],
    'web': ['nikl_web/', read_text_corpus],
    'spoken': ['nikl_spoken/', read_text_corpus],
    'written': ['nikl_written/', read_text_corpus],
    'news': ['nikl_news/', read_text_corpus],
    'petitions': ['korean_petitions', read_Korpora_corpus],
    'GT': ['GT', read_gt_corpus],
    'nikl_spoken': ['GT/nikl_spoken', read_gt_corpus],
    'aihub_multimodal': ['GT/aihub_multimodal', read_gt_corpus],
    'aihub_multimodal_video': ['GT/aihub_multimodal_video', read_gt_corpus],
    'mindslabQA': ['GT/mindslabQA', read_gt_corpus],
    'SERW': ['GT/nikl_spoken/SERW.json', read_gt_corpus],
    'SARW': ['GT/nikl_spoken/SARW.json', read_gt_corpus]
}


class DataSetter():
    """
    train_test_val_split = 8:1:1
    """

    def __init__(self, cfg):
        self.cfg = cfg
        self.data_path = cfg.datadir
        self.dest_path = cfg.destdir
        self.corpora = cfg.corpora
        print(self.corpora)

        self.all_corpora = []
        train_, val_, test_ = [], [], []
        corporafile = 'corpora.pickle'
        # corporafile = Path(self.data_path) / corporafile

        all_corpora, size_list = self.find_corpora_path()
        total_corpus_num = sum(size_list)
        train_corpora = []
        val_corpora = []
        test_corpora = []
        for corpus in all_corpora:
            total_num = len(corpus)
            print('total num of corpus', total_num)
            if cfg.test_p:
                test_corpora.extend(corpus[: int(cfg.test_p * total_num)])
                corpus = corpus[int(cfg.test_p * total_num):]
            if cfg.val_p:
                val_corpora.extend(corpus[: int(cfg.val_p * total_num)])
                corpus = corpus[int(cfg.val_p * total_num):]
            train_corpora.extend(corpus)

        if cfg.trainpref:
            train_file = Path(self.data_path, cfg.trainpref)
            train_file.mkdir(parents=True, exist_ok=True)
            train_file /= corporafile
            with open(train_file, 'wb') as f:
                pickle.dump(train_corpora, f)
                print(f'loading last converted corpora blocks at {corporafile}')
        if cfg.validpref:
            val_file = Path(self.data_path, cfg.validpref)
            val_file.mkdir(parents=True, exist_ok=True)
            val_file /= corporafile
            with open(val_file, 'wb') as f:
                pickle.dump(val_corpora, f)
                print(f'loading last converted corpora blocks at {corporafile}')
        if cfg.testpref:
            test_file = Path(self.data_path, cfg.testpref)
            test_file.mkdir(parents=True, exist_ok=True)
            test_file /= corporafile
            with open(test_file, 'wb') as f:
                pickle.dump(test_corpora, f)
                print(f'loading last converted corpora blocks at {corporafile}')

    def find_corpora_path(self):
        # pickle file 있는지 확인하는 함수

        def make_doc_list(corpus_list, result_list, size_list):
            for i, corpus in enumerate(corpus_list):
                corpus = corpus.strip()
                corpus_name, fn = CORPUS_DIR[corpus]
                doc_list = fn(corpus_name)
                result_list.append(doc_list)
                size_list.append(len(doc_list))
                print(corpus, '==>', len(doc_list))

        all_corpora = []
        size_list = []

        if self.corpora['File'] is not None:
            print('load from file')
            for_files = [] + self.corpora['File'].split(',')
            make_doc_list(for_files, all_corpora, size_list)

        if self.corpora['Kopora'] is not None:
            print('load from Korpora')
            for_kopora = [] + self.corpora['Kopora'].split(',')
            make_doc_list(for_kopora, all_corpora, size_list)
        return all_corpora, size_list


class Processor():
    """
    korpus 종류에 맞춰서 preprocessing 해야 함
    """

    def __init__(self, cfg, data_path):
        self.cfg = cfg
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'  # 병렬처리
        self.gpus = torch.cuda.device_count()
        self.workers_num = cfg.workers if cfg.workers is not None else mp.cpu_count() // 2
        self.data_path = data_path
        self.dest_path = Path(cfg.destdir)
        self.corpora = cfg.corpora

        self.all_corpora = []
        corporafile = f'corpora.pickle'
        corporafile = Path(self.data_path, corporafile)
        assert corporafile.exists(), f'there is no corpora.pickle file yet. try "split_data" : True on "preprocessing.yml"'
        print(f'loading converted corpora file at {corporafile}_print script')
        with open(corporafile, 'rb') as f:
            self.all_corpora = pickle.load(f)

        self.doc_num = len(self.all_corpora)

        self.datasets = []

        sp = spm.SentencePieceProcessor(model_file=self.cfg.spmfile)
        self.tokenizer = MCSTokenizer(tagger=Mecab(), change=Change(), sp=sp)

    def save_block(self, chunk_corpora, idx, mode):
        proc = os.getpid()
        proc_num = f'{idx}_{proc}'

        logger.info(f'[{proc}] | processing data num: {len(chunk_corpora)}')
        self.ds.save_blocks(chunk_corpora, proc_num, mode)
        print(f'[{proc}] | average sentence num: {self.ds.get_average_sent_num()}')

    def process(self, mode):
        print(self.cfg)
        dest_dir = self.dest_path / mode
        dest_dir.mkdir(parents=True, exist_ok=True)

        token_dicts = {
            'cls_token': '<cls>',
            'cls_id': self.tokenizer.s.PieceToId('<cls>'),
            'sep_token': '<eos>',
            'sep_id': self.tokenizer.s.PieceToId('<eos>'),
            'pad_token': '<pad>',
            'pad_id': self.tokenizer.s.PieceToId('<pad>'),
            'global_token': '<glb>',
            'global_id': self.tokenizer.s.PieceToId('<glb>')
        }
        if 'utterance' in self.cfg.data_type:
            # token_dicts['cls_token'] = '<noi>'
            # token_dicts['cls_id'] = self.tokenizer.s.PieceToId('<noi>')
            token_dicts['bos_token'] = '<sos>'
            token_dicts['bos_id'] = self.tokenizer.s.PieceToId('<sos>')

        if 'utterance' in self.cfg.data_type:
            if 'gt' in self.cfg.data_type:
                ds = BlockedGTDataset
            else:
                ds = BlockedPLATODataset
        else:
            ds = BlockedRawPlaintextDataset

        self.ds = ds(
            self.cfg,
            self.tokenizer,
            **token_dicts
        )
        if self.cfg.save_file:
            filenum = len(list(dest_dir.glob('*')))
            assert filenum == 0, f'# {filenum} files in {dest_dir} ... '

            chunk_num = math.ceil(len(self.all_corpora) / self.cfg.workers)
            chunk_corpora = [self.all_corpora[i * chunk_num: min((i + 1) * chunk_num, len(self.all_corpora) - 1)]
                             for i in range(self.cfg.workers)]
            print([len(c) for c in chunk_corpora])
            procs = []

            for i, idx in enumerate(range(len(chunk_corpora))):
                proc = Process(target=self.save_block, args=(chunk_corpora[idx], i, mode))
                procs.append(proc)
                proc.start()

            for proc in procs:
                proc.join()

        logger.info('| Wrote preprocessed data to {}'.format(self.cfg.destdir))


# define and read arguments especially for running, not modeling
def parse_args():
    parser = argparse.ArgumentParser()

    # Arguments for running.
    parser.add_argument('--run', type=str)
    parser.add_argument('--log_level', type=str, default='INFO', choices=['INFO', 'DEBUG'])

    # Configuration files for training
    parser.add_argument('--cfg', type=str)

    args = parser.parse_args()
    return args


def setup_logger(cfg):
    log_file_name = '{}_{}.log'.format(datetime.now().strftime('%y%m%d_%H%M%S'), cfg.args.run)
    log_file_path = Path(cfg.const.log_dir, cfg.model_name, log_file_name)
    log_file_path.parent.mkdir(parents=True, exist_ok=True)

    logging.basicConfig(level=logging.DEBUG)
    formatter = logging.Formatter('[%(levelname)s|%(filename)s:%(asctime)s] %(message)s')

    # create file handler
    fh = logging.FileHandler(str(log_file_path))
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)

    # create stderr handler
    ch = logging.StreamHandler()  # stderr by default
    ch.setLevel(cfg.args.log_level)
    ch.setFormatter(formatter)

    handlers = [fh, ch]
    logging.getLogger('').handlers = handlers


# Setup configuration
def setup_config(args):
    # Set model_dir_path
    cfg = OmegaConf.load(args.cfg)

    data_dir_path = Path(cfg.destdir)
    data_dir_path.mkdir(parents=True, exist_ok=True)

    cfg.args = vars(args)
    cfg.const = const
    return cfg


if __name__ == '__main__':
    args = parse_args()
    cfg = setup_config(args)
    # setup_logger(cfg)

    if cfg.split_data:
        DataSetter(cfg)

    if cfg.save_file:
        corpora = []
        if cfg.testpref:
            corpora.append(Path(cfg.datadir, cfg.testpref))
        if cfg.validpref:
            corpora.append(Path(cfg.datadir, cfg.validpref))
        corpora.append(Path(cfg.datadir, cfg.trainpref))
        for pickle_path in corpora:
            mode = pickle_path.parts[-1]
            print('processing mode: ', mode)
            processor = Processor(cfg, pickle_path)
            processor.process(mode)


