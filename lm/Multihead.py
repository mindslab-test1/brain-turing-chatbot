import torch.nn as nn

from .Attention import Attention


class MultiheadFull(nn.Module):
    def __init__(self, args):
        super().__init__()
        n_head = args.multi_heads
        d_q = args.hidden_size
        d_head = args.hidden_size // args.multi_heads

        self.n_head = n_head
        self.w_q = nn.Linear(d_q, n_head * d_head)
        self.w_k = nn.Linear(d_q, n_head * d_head)
        self.w_v = nn.Linear(d_q, n_head * d_head)
        self.f_c = nn.Linear(n_head * d_head, d_q)

        self.attention = Attention(args=args)

        self.layer_norm = nn.LayerNorm(d_q, eps=1e-6)
        self.dropout = nn.Dropout(p=args.dropout)

    def forward(self, q, k, v, attention_weight_bias=None, mask=None):
        # q : [batch_size, seq_len, d_model]
        bsz = q.size(0)
        q_l = q.size(1)
        k_l = k.size(1)
        v_l = v.size(1)

        q, k, v = map(self.layer_norm, (q, k, v))
        q, k, v = self.w_q(q), self.w_k(k), self.w_v(v)

        def multihead(x, l):
            return x.reshape(bsz, l, self.n_head, -1).transpose(1, 2)

        q, k, v = multihead(q, q_l), multihead(k, k_l), multihead(v, v_l)

        q, attn = self.attention(q, k, v, attention_weight_bias, mask=mask)
        q = q.transpose(1, 2).reshape(bsz, q_l, -1)
        q = self.dropout(self.f_c(q))

        return q, attn
