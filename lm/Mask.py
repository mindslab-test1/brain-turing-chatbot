import torch
import random


class Mask:
    def __init__(self, stride, c, w, r):
        """
        :param stride: stride for sparsefix
        :param c: number of columns(pillar) for sparsefix
        :param w: window size for bigbird
        :param r: random number for bigbird
        """
        super().__init__()
        self.stride = stride
        self.c = c
        self.w = w
        self.r = r

    def sparsefix(self, seq_len):
        mask = torch.zeros([seq_len, seq_len])
        for i in range(self.stride - 1, self.stride - self.c - 1, -1):
            mask[:, i::self.stride] = 1
        for i in range(seq_len):
            row = i // self.stride
            mask[i, self.stride * row:self.stride * (row + 1)] = 1
            mask[i, i + 1:] = 0
        mask[-1:] = 1
        return mask
    
    def bigbird(self, seq_len):
        mask = torch.zeros([seq_len, seq_len])
        mask[-1:, :] = 1
        mask[:, -1:] = 1
        for i in range(seq_len - 1):
            mask[i, max(0, i - self.w):min(seq_len, i + self.w + 1)] = 1
            for j in random.sample([k for k in range(seq_len)], k=self.r):
                mask[i, j] = 1
        return mask
    
    def mask(self, seq, noi_id, pad_id):
        """
        :param seq: 1-dim list or tensor
        :param sos_id: start of sequence token
        :param eos_id: end of sequence token
        :return: mask tensor (bool type, not yet cuda)
        """

        if not isinstance(seq, torch.Tensor):
            seq = torch.tensor(seq)
        seq_len = seq.size(0)

        assert seq.dim() == 1, "dim() must be 1"

        noi_idx_list = [0] + ((seq == noi_id).nonzero(as_tuple=True)[0] + 1).tolist()
        pad_len = len((seq == pad_id).nonzero(as_tuple=True)[0])
        length = len(noi_idx_list) - 1
        out_mask = torch.zeros([0, seq_len])
        line = random.randint(0, length)
        for idx in range(length):
            row_len = noi_idx_list[idx+1] - noi_idx_list[idx]
            now_mask = torch.zeros([row_len, 0])
            for ix in range(length):
                if idx == ix:
                    if idx < line:
                        mask = self.bigbird(row_len)
                    else:
                        mask = self.sparsefix(row_len)
                else:
                    col_len = noi_idx_list[ix+1] - noi_idx_list[ix]
                    mask = torch.zeros([row_len, col_len])
                    if (idx < line and ix < line) or idx > ix:
                        mask[:, -1:] = 1
                now_mask = torch.cat([now_mask, mask], dim=-1)
            
            row_pad_mask = torch.zeros([row_len, pad_len])
            now_mask = torch.cat([now_mask, row_pad_mask], dim=-1)
            out_mask = torch.cat([out_mask, now_mask], dim=0)
        col_pad_mask = torch.zeros([pad_len, seq_len])
        out_mask = torch.cat([out_mask, col_pad_mask], dim=0)
        return out_mask.bool()


if __name__ == "__main__":
    m = Mask(2, 1, 1, 2)
    test = [0, 1, 2, 27, 15, 3, 4, 7, 0, 5, 6, 81, 0, 1, 5, 7, 8, 8]
    out_mask = m.mask(test, noi_id=7, pad_id=8)
    print(out_mask)
