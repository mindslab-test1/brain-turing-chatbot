import random

import torch
import torch.nn as nn
import torch.nn.functional as F
import tqdm

from .Block import Encoder  # , Decoder
from .Embedding import Embedding, UnEmbedding, Addition


class Transformer(nn.Module):
    def __init__(self, args, mcst):
        super().__init__()
        self.args = args

        # maybe no use...?
        '''
        self.mcst = mcst
        
        self.pad_id = mcst.s.pad_id()
        self.eos_id = mcst.s.eos_id()
        self.dec_seq_len = args.dec_seq_len
        
        def sparsefix(seq_len, stride=128, c=8):
            mask = torch.zeros([seq_len, seq_len])
            for i in range(stride - 1, stride - c - 1, -1):
                mask[:, i::stride] = 1
            for i in range(seq_len):
                row = i // stride
                mask[i, stride * row:stride * (row + 1)] = 1
                mask[i, i + 1:] = 0
            return mask.bool()

        if args.enc_mask == "sparsefix":
            self.register_buffer('enc_sparsefix', sparsefix(args.enc_seq_len))
        if args.dec_mask == "sparsefix":
            self.register_buffer('dec_sparsefix', sparsefix(args.dec_seq_len))
        '''
        self.embed = Embedding(args=args, vocab=mcst.s)
        # self.add_embed = Addition(args=args) if args.add_embed else None
        self.encoder = Encoder(args=args)
        # self.decoder = Decoder(args=args)
        self.unembed = UnEmbedding(args=args, vocab=mcst.s)

        # coverage mechanism from Pointer-Generator net
        '''
        self.gen = nn.Sequential(
            nn.Linear(args.hidden_size * 2 + mcst.s.vocab_size(), 1),
            nn.Sigmoid()
        )
        '''
        for p in self.parameters():
            if p.dim() > 1:
                nn.init.xavier_uniform_(p)

        # embedding-unembedding layer sharing
        '''
        self.unembed.untoken.unlinear.weight.data = self.embed.token.linear.weight.data.t()
        self.unembed.untoken.unembed.weight.data = self.embed.token.embed.weight.data
        '''
    def get_pad_mask(self, seq, pad_idx):
        bsz = seq.size(0)
        mask = seq != pad_idx
        mask = mask.reshape(bsz, 1, 1, -1)
        return mask

    # bigbird mask pattern
    '''
    def bigbird(self, seq_len, g=2, w=2, r=3):
        mask = torch.zeros([seq_len, seq_len])
        mask[:g, :] = 1
        mask[:, :g] = 1
        for i in range(g, seq_len):
            mask[i, max(0, i - w):min(seq_len, i + w)] = 1
            for j in random.sample([k for k in range(seq_len)], k=r):
                mask[i, j] = 1
        return mask.bool()
    '''
    def forward(self, enc, dec=None, add=None):
        # only pad mask
        encdec_mask = self.get_pad_mask(enc, self.pad_id)

        # I have no idea about mask pattern yet!
        '''
        if self.args.enc_mask == "bigbird":
            enc_mask = encdec_mask & self.bigbird(enc.size(1)).cuda()
        elif self.args.enc_mask == "sparsefix":
            enc_mask = encdec_mask & self.enc_sparsefix
        else:
            enc_mask = encdec_mask
        '''
        enc_mask = encdec_mask

        enc = self.embed(enc)
        '''
        if add is not None:
            add = self.add_embed(add)
            enc += add
        '''
        enc, enc_attn = self.encoder(enc, enc_mask)
        tar = self.unembed(enc)

        # maybe no use decoder!
        '''
        if dec is not None:
            dec_mask = self.get_pad_mask(dec, self.pad_id)
            if self.args.dec_mask == "bigbird":
                dec_mask = dec_mask & self.bigbird(dec.size(1)).cuda()
            elif self.args.dec_mask == "sparsefix":
                dec_mask = dec_mask & self.dec_sparsefix

            dec = self.embed(dec, enc=False)
            dec_in = dec
            dec, post_attn, self_attn, cross_attn = self.decoder(enc, encdec_mask, dec, dec_mask)
            dec_out = dec
            dec = self.unembed(dec)

            post_attn = self.unembed(post_attn)
            pgen = torch.cat([dec, dec_in, dec_out], dim=-1)
            pgen = self.gen(pgen)
            dec = post_attn * (1 - pgen) + dec * pgen

            cov = self.coverage(cross_attn).mean()
        else:
            cov = None
        '''
        cov = None
        return tar, enc, dec, cov

    # coverage loss in coverage mechanism from Pointer-Generator net
    '''
    def coverage(self, attention):
        cov = attention.cumsum(dim=-2)
        cov = torch.cat([torch.zeros_like(cov[:, :, :1]), cov[:, :, :-1]], dim=-2)
        cov = torch.min(attention, cov)
        return cov
    '''

    # inference with lots of sampling methods! but, not yet~
    '''
    def inference(self, enc=None, dec=None, k=40, p=0.98, t=0.5, lp=0.65, cp=0.1, ep=0.1):
        # enc : [1, 8192], dec : [1, 1]
        encdec_mask = self.get_pad_mask(enc, self.pad_id)
        if self.args.enc_mask == "bigbird":
            enc_mask = encdec_mask & self.bigbird(enc.size(1)).cuda()
        elif self.args.enc_mask == "sparsefix":
            enc_mask = encdec_mask & self.enc_sparsefix
        else:
            enc_mask = encdec_mask

        enc = self.embed(enc)
        enc, attn = self.encoder(enc, enc_mask)

        # Beam Search
        sequences = [[dec, dec.new_tensor(1)]]
        length_tqdm = tqdm.tqdm(range(1, self.dec_seq_len))
        end_k = []
        swc = 0
        for sl in length_tqdm:
            if dec[0, sl] != self.eos_id and swc == 0:
                continue
            else:
                swc = 1
            all_candidates = []

            for seq in sequences:
                dec, score = seq  # dec : [1, sl]
                dec_mask = self.get_pad_mask(dec, self.pad_id)
                if self.args.dec_mask == "bigbird":
                    dec_mask = dec_mask & self.bigbird(dec.size(1)).cuda()
                elif self.args.dec_mask == "sparsefix":
                    dec_mask = dec_mask & self.dec_sparsefix

                out = self.embed(dec, enc=False)  # out : [1, sl, hs]
                dec_in = out

                out, post_attn, _, cross_attn = self.decoder(enc, encdec_mask, out, dec_mask=dec_mask)  # out : [1, sl, hs]
                dec_out = out
                out = self.unembed(out)  # out : [1, sl, vs]
                post_attn = self.unembed(post_attn)

                pgen = torch.cat([out, dec_in, dec_out], dim=-1)
                pgen = self.gen(pgen)
                out = post_attn * (1 - pgen) + out * pgen
                if cp != 0.0:
                    cov = self.coverage(cross_attn[:, :, :sl]).mean(dim=1).mean(dim=-1)[0, -1]
                    score = score + cp * cov

                out[0, sl - 1, self.eos_id] += sl * ep
                if t != 0.0:
                    t = 1
                vle = F.softmax(out[0, sl - 1] / t, dim=-1)
                vle, idx = vle.sort(descending=True)
                cumsum = .0
                for v, j in zip(vle, idx):
                    dec_clone = dec.clone()
                    dec_clone[0, sl] = j
                    jeomsu = score * (1 / v + torch.log(v))
                    if jeomsu.isnan():
                        continue
                    all_candidates.append([dec_clone, jeomsu])
                    cumsum += v
                    if cumsum > p:
                        break

            ordered = sorted(all_candidates, key=lambda tup: tup[1])
            sequences = []
            for seq in ordered:
                if seq[0][0, sl] == self.eos_id:
                    seq[1] /= ((5 + sl) / (5 + 1)) ** lp
                    end_k.append(seq)
                else:
                    sequences.append(seq)
                if len(sequences) >= k or len(end_k) >= k:
                    break
            if len(end_k) >= k:
                break

            length_tqdm.write(f"end_k : {len(end_k)} / {k}, {sl}th text")
            for seq in sequences:
                length_tqdm.write(f"{seq[1].data:3.6f} : {self.mcst.s.Decode(seq[0][0].tolist())[-128:]}")
            length_tqdm.write("\n")

        if len(end_k) > 0:
            return sorted(end_k, key=lambda tup: tup[1])[0][0]
        else:
            return sequences[0][0]
    '''
