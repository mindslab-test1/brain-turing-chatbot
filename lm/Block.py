import torch.nn as nn
from torch.utils.checkpoint import checkpoint as gdcp

from .Feedforward import Feedforward
from .Multihead import MultiheadFull


class EncoderBlock(nn.Module):
    def __init__(self, args, shra, fefo):
        super().__init__()

        self.shra = shra if shra is not None else MultiheadFull(args=args)
        self.fefo = fefo

    def forward(self, enc, enc_mask, attention_weight_bias=None, inference=True):
        # TODO: Can't we just use eval?
        if inference:
            out, attn = self.shra(enc, enc, enc, attention_weight_bias, enc_mask)
            enc += out
            enc += self.fefo(enc)
        else:
            out, attn = gdcp(self.shra, enc, enc, enc, attention_weight_bias, enc_mask)
            # out, attn = self.shra(enc, enc, enc, enc_mask)
            enc = enc + out
            out = gdcp(self.fefo, enc)
            # out = self.fefo(enc)
            enc = enc + out
        return enc, attn


class Encoder(nn.Module):
    def __init__(self, args):
        super().__init__()

        self.shra = MultiheadFull(args=args) if args.enc_shra_sharing else None
        self.blocklist = nn.ModuleList(
            [EncoderBlock(args=args, shra=self.shra, fefo=Feedforward(args=args)) for _ in
             range(args.enc_n_layers)])
        self.layer_norm = nn.LayerNorm(args.hidden_size, eps=1e-6)

    def forward(self, enc, enc_mask, attention_weight_bias=None, inference=True):
        attn = None
        for block in self.blocklist:
            enc, attn = block(enc, enc_mask, attention_weight_bias, inference)

        enc = self.layer_norm(enc)
        return enc, attn
