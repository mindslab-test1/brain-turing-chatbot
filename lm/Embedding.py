import torch, math
import torch.nn as nn
from typing import Optional, List


class Embedding(nn.Module):
    def __init__(self, args, vocab):
        super().__init__()

        self.token = Token(args=args, vocab=vocab)

        if args.segment_embedding == 'fixed_embedding_matrix':
            print('Using fixed_embedding_matrix for segment embedding.')
            self.segment_embedding = SegmentEmbedding(max_segment_length=args.max_segment_length, hidden_size=args.hidden_size)
        elif args.segment_embedding == 'linear':
            print('Using linear vector for segment embedding.')
            self.segment_embedding = LinearSegmentEmbedding(hidden_size=args.hidden_size)
        
        if args.absolute_position_encoding == 'tied_absolute_matrix':
            print('Using tied_absolute_matrix for position encoding.')
            self.position_embedding = TiedAbsoluteLearnableEmbedding(max_sequence_length=args.max_token_len_per_segment, hidden_size=args.hidden_size)
        elif args.absolute_position_encoding == 'tied_absolute_linear':
            print('Using tied_absolute_linear for position encoding.')
            self.position_embedding = LinearPositionEmbedding(args.hidden_size)

    def forward(self, token_indices, segment_indices, position_indices=None):
        if hasattr(self, 'position_embedding') and position_indices is not None:
            return self.token(token_indices) + self.segment_embedding(segment_indices) + self.position_embedding(position_indices)
        else: 
            return self.token(token_indices) + self.segment_embedding(segment_indices) 


class MLMHead(nn.Module):
    def __init__(self, embed_size, hidden_size, vocab_size):
        super().__init__()
        self.layer_norm = nn.LayerNorm(embed_size)
        self.hidden_to_embed = nn.Linear(hidden_size, embed_size)
        self.embed_to_vocab = nn.Linear(embed_size, vocab_size)
        self.activation = nn.GELU()

    def forward(self, hidden_states):
        hidden_states = self.hidden_to_embed(hidden_states)
        hidden_states = self.activation(hidden_states)
        hidden_states = self.layer_norm(hidden_states)
        hidden_states = self.embed_to_vocab(hidden_states)

        return hidden_states


class Token(nn.Module):
    def __init__(self, args, vocab):
        super().__init__()

        self.embed = nn.Embedding(vocab.vocab_size(), args.embed_size, padding_idx=vocab.pad_id())
        self.linear = nn.Linear(args.embed_size, args.hidden_size)

    def forward(self, x):
        return self.linear(self.embed(x))


'''
class Position(nn.Module):
    def __init__(self, args):
        super().__init__()
        if args.enc_seq_len > 0:
            self.register_buffer('enc_pos_table', self.get_sinusoid_encoding_table(args.enc_seq_len, args.hidden_size))
        if args.dec_seq_len > 0:
            self.register_buffer('dec_pos_table', self.get_sinusoid_encoding_table(args.dec_seq_len, args.hidden_size))

    def forward(self, x, enc=True):
        if enc:
            return self.enc_pos_table[:, :x.size(1)].clone().detach()
        else:
            return self.dec_pos_table[:, :x.size(1)].clone().detach()
    @staticmethod
    def get_sinusoid_encoding_table(seq_len, d_hidden):
        def get_position_angle_vec(position, d_model):
            return [position / np.power(10000, 2 * (j // 2) / d_model) for j in range(d_model)]

        sinusoid_table = np.array([get_position_angle_vec(i, d_hidden) for i in range(seq_len)])
        sinusoid_table[:, 0::2] = np.sin(sinusoid_table[:, 0::2])
        sinusoid_table[:, 1::2] = np.cos(sinusoid_table[:, 1::2])
        return torch.FloatTensor(sinusoid_table).unsqueeze(0)
'''


class LinearSegmentEmbedding(nn.Module):
    def __init__(self, hidden_size):
        super().__init__()
        self.embedding = nn.Linear(1, hidden_size)
    
    def forward(self, segment_indices):
        # (batch_size, seq_len) -> (batch_size, seq_len, hidden_size)
        return self.embedding(segment_indices.unsqueeze(-1).float())


class LinearPositionEmbedding(nn.Module):
    def __init__(self, hidden_size):
        super().__init__()
        self.embedding = nn.Linear(1, hidden_size)
    
    def forward(self, position_indices):
        # (batch_size, seq_len) -> (batch_size, seq_len, hidden_size)
        return self.embedding(position_indices.unsqueeze(-1).float())


class SegmentEmbedding(nn.Module):
    def __init__(self, max_segment_length, hidden_size):
        super().__init__()

        self.embedding = nn.Embedding(max_segment_length, hidden_size)

    def forward(self, x):
        return self.embedding(x)


class TiedAbsoluteLearnableEmbedding(nn.Module):
    def __init__(self, max_sequence_length, hidden_size):
        super().__init__()
        self.position_embedding = nn.Embedding(max_sequence_length, hidden_size)
    
    def forward(self, position_indices):
        return self.position_embedding(position_indices)


class UntiedAbsolutePositionEmbedding(nn.Module):
    '''A torch moudule for computing positional attention logits.'''
    def __init__(
            self, 
            max_position_len: int,
            hidden_size: int, 
            num_attention_heads: int,
            attention_scale_factor: float=2.0
        ) -> torch.Tensor:
        '''Initializes an instance of the class with the model's hidden size and atention heads count and other model related hparams.
        :param max_position_len: The maximum number of tokens including the reserved token reset values. (E.g. in the case of BERT it would be 511 + 1 (=512).)
        :type max_positon_len: int
        :param hidden_size: The hidden size of the model.
        :type hidden_size: int
        :param num_attention_heads: The number of attention heads the model has.
        :type num_attention_heads: int
        :param attention_scale_factor: The scaler that is to be multiplied to hidden_size / num_attention_heads when scaling the attention logits.
        :type attention_scale_factor: int
        '''

        super().__init__()
        assert hidden_size % num_attention_heads == 0
        self.scaling_factor = float((attention_scale_factor * hidden_size / num_attention_heads)) ** (-0.5)
        self.num_attention_heads = num_attention_heads
        self.max_utterance_len = max_position_len 
        self.layer_norm = nn.LayerNorm(hidden_size)
        self.position_embedding = nn.Embedding(max_position_len, hidden_size)
        self.position_query = nn.Linear(hidden_size, hidden_size)
        self.position_key = nn.Linear(hidden_size, hidden_size)

    def forward(
        self, 
        position_indices: torch.Tensor, 
        untied_reserved_token_masks: Optional[torch.Tensor]=None
        ) -> torch.Tensor:
        '''Converts absolute position indices to their attention logits.

        Caculates the attention logits for each of the position indices and optionally resets some (or all) of the reserved token related logits using the accompanying masks.

        :param position_indices: A tensor of shape (batch_size, seq_len) containing the positions indices.
        :type position indices: torch.Tensor
        :param untied_reserved_token_masks: A tensor of shape (batch_size, seq_len, seq_len, num_masks). Set to False if the corresponding element should be reset, otherwise set to True.
        :type untied_reserved_token_masks: torch.Tensor
        :return: Attention logits computed from position embeddings in the shape of (batch_size, num_heads, seq_len, seq_len)
        :rtype: torch.Tensor
        '''
        # (batch_size, seq_len + reserved_len, hidden_dim)
        position_embeddings = self.position_embedding(position_indices)
        # (batch_size, seq_len + reserved_len, hidden_dim)
        position_embeddings = self.layer_norm(position_embeddings) 

        batch_size = position_indices.size(0)
        seq_len = position_indices.size(1)
        # (batch_size, seq_len + reserved_len, hidden_dim)
        position_query = self.position_query(position_embeddings) 
        # (batch_size, num_heads, seq_len + reserved_len, hidden_size)
        position_query = position_query.view(batch_size, seq_len, self.num_attention_heads, -1).transpose(1, 2)
        # (batch_size, seq_len + reserved_len, hidden_dim)
        position_key = self.position_key(position_embeddings) 
        # (batch_size, num_heads, seq_len + reserved_len, hidden_size)
        position_key = position_key.view(batch_size, seq_len, self.num_attention_heads, -1).transpose(1, 2)
        # (batch_size, num_heads, seq_len + reserved_len, seq_len + reserved_len)
        entire_position_logits = self.scaling_factor * torch.matmul(position_query, position_key.transpose(2, 3))

        if untied_reserved_token_masks is None:
            return entire_position_logits
        else:
            # (batch_size, num_heads, seq_len, seq_len)
            num_masks = untied_reserved_token_masks.size(-1)
            actual_position_logits = entire_position_logits[..., num_masks:, num_masks:]

            # (batch_size, 1, seq_len, seq_len, num_masks)
            untied_reserved_token_masks = untied_reserved_token_masks.unsqueeze(1)

            # (batch_size, num_heads, seq_len + reserved_len) -> (batch_size, num_heads, 1, 1, reserved_len)  
            reserved_token_reset_values = torch.diagonal(entire_position_logits, offset=0, dim1=-2, dim2=-1)[..., None, None, :num_masks]
            # (batch_size, 1, seq_len, seq_len, num_masks), (batch_size, num_heads, 1, 1, reserved_len) -> (batch_size, num_heads, seq_len, seq_len)
            reserved_token_reset_values = torch.sum(~untied_reserved_token_masks * reserved_token_reset_values, -1)
            actual_position_logits = actual_position_logits.where(torch.sum(~untied_reserved_token_masks, -1) == 0, reserved_token_reset_values)

            return actual_position_logits

            
class T5RelativePositionEmbedding(nn.Module):
    '''A torch module that is responsible for T5 relative position encoding related tasks.'''
    def __init__(self, num_relative_position_bins: int, num_attention_heads: int):
        '''Initializes an embedding matrix of dimension (num_relative_bins, num_attention_heads).
        :param num_relative_position_bins: The number of bins for the relative position indices including the reset values.
        :type num_relative_position_bins: int
        :param num_attention_heads: The number of attention heads the transformer model contains.
        :type num_attention_heads: int
        '''
        super().__init__()
        self.embedding = nn.Embedding(num_relative_position_bins, num_attention_heads)

    def forward(self, relative_position_indices: torch.Tensor) -> torch.Tensor:
        '''Maps the relative position indices to their attention logits.
        :param relative_position_indices: A tensor of shape (batch_size, seq_len) containing the relative position indices for the given sequence(s).
        :type relative_position_indices: torch.Tensor
        :return: A tensor of shape (batch_size, num_attention_heads, seq_len, seq_len)
        :rtype: torch.Tensor
        '''
        # (batch_size, seq_len, seq_len) -> (batch_size, seq_len, seq_len, num_heads)
        relative_position_logits = self.embedding(relative_position_indices)
        # (batch_size, seq_len, seq_len, num_heads) -> (batch_size, num_heads, seq_len, seq_len)
        return relative_position_logits.permute(0, 3, 1, 2)

    @staticmethod
    # TODO: figure out why num_buckets // 2 is never used in some of the cases (if not always)
    def create_t5_relative_position_encoding_matrix(sequence_len: int, num_buckets: int, max_distance: int) -> torch.Tensor:
        '''Creates a relative position indices matrix of shape (maximum sequence length, maximum sequence length)
        :param sequence_len: The maximum sequence length.
        :type sequence_len: int
        :param num_buckets: The number of buckets (bins) to which an index can belong.
        :type num_buckets: int
        :param max_distance: The maximum distance of the relative position indices after which the indices will have the same values.
        :type max_distance: int
        :return: A relative position indices matrix of shape (sequence_len, sequencec_len).
        '''
        # make a relative postion matrix
        relative_position_indices = torch.arange(sequence_len)[None, :] - torch.arange(sequence_len)[:, None] 

        right_half_offset = num_buckets // 2
        right_half_offset_matrix = (relative_position_indices > 0).long() * right_half_offset

        symetric_relative_position_indices = torch.abs(relative_position_indices)

        num_increment_by_one_buckets = right_half_offset // 2
        within_increament_by_one_range = symetric_relative_position_indices < num_increment_by_one_buckets 

        bucket_range_normalized_position = torch.log(symetric_relative_position_indices.float() / num_increment_by_one_buckets)
        bucket_range_normalized_max_distance = math.log(max_distance / num_increment_by_one_buckets)
        num_exponential_buckets = right_half_offset - num_increment_by_one_buckets 

        normalized_bucket_indices = (bucket_range_normalized_position / bucket_range_normalized_max_distance * num_exponential_buckets).long()

        relative_position_for_exponential_bucket_indices = num_increment_by_one_buckets + normalized_bucket_indices

        # set the indices whose values exceed max_distance to the last bucket indice
        relative_position_for_exponential_bucket_indices = torch.min(
            relative_position_for_exponential_bucket_indices, 
            torch.full_like(relative_position_for_exponential_bucket_indices, right_half_offset - 1)
        )

        # combine increment-by-one and exponential-increment results
        symetric_relative_position_indices = torch.where(
            within_increament_by_one_range, 
            symetric_relative_position_indices, 
            relative_position_for_exponential_bucket_indices
        )

        # add offset to the upper right triangular part
        bucketized_relative_position_indices = right_half_offset_matrix + symetric_relative_position_indices

        return bucketized_relative_position_indices


# TODO: Generalize so that a list of global token reset values are sufficient to compute the matrix. (Is it even possible?)
def create_reserved_token_position_logit_reset_matrix(
    global_token_indices: List[int], 
    sequence_len: int, 
    global_to_internal: Optional[int]=None, 
    global_to_external_global: Optional[int]=None, 
    normal_to_internal_global: Optional[int]=None, 
    normal_to_external_global: Optional[int]=None, 
    fill_empty_space_with: Optional[int]=-1,
) -> torch.Tensor:
    '''Resets the position indices matrix according to the global token indices.
    :param global_token_indices: A list of global token indices.
    :type global_token_indices: List[int]
    :param sequence_len: The length of the sequence.
    :type sequence_len: int
    :param global_to_internal: The value to fill the global to normal word attention logit positions including the "global to itself" logits.
    :type global_to_internal: int
    :param global_to_external_global: The value to fill the global to other external global attention logit positions.
    :type global_to_external: int
    :param normal_to_internal_global: The value to fill the normal token to their internal global token attention logit positions.
    :type normal_to_internal_global: int
    :param normal_to_external_global: The value to fill the normal token to external global token attention logit positions.
    :type normal_to_external_global: int
    :param fill_empty_space_with: The value to fill the positions that are not reset by the any of the reset values.
    :type fill_empty_space_with: int
    :return: A matrix whose reserved token logit positions are specified by the given reset values.
    :rtype: torch.Tensor
    '''
    mask = torch.full((sequence_len, sequence_len), dtype=torch.long, fill_value=fill_empty_space_with)

    # TODO: Check for inefficiency and correctness.
    prev_idx = -1
    for cur_idx in global_token_indices:
        indices_wo_cur_idx = [idx for idx in global_token_indices if idx != cur_idx]
        # filtering is not really necessary in the below line since the value will be overridden anyway.
        if global_to_external_global is not None:
            mask[cur_idx, indices_wo_cur_idx] = global_to_external_global
        if global_to_internal is not None:
            mask[cur_idx, prev_idx + 1 : cur_idx + 1] = global_to_internal
        if normal_to_internal_global is not None:
            mask[prev_idx + 1 : cur_idx, cur_idx] = normal_to_internal_global
        if normal_to_external_global is not None:
            mask[prev_idx + 1 : cur_idx, indices_wo_cur_idx] = normal_to_external_global
        prev_idx = cur_idx

    return mask
