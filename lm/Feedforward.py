import torch.nn as nn
import torch.nn.functional as F


class Feedforward(nn.Module):
    def __init__(self, args):
        super().__init__()

        self.w_1 = nn.Linear(args.hidden_size, args.hidden_size * 4)
        self.w_2 = nn.Linear(args.hidden_size * 4, args.hidden_size)
        self.dropout = nn.Dropout(p=args.dropout)
        self.layer_norm = nn.LayerNorm(args.hidden_size, eps=1e-6)

    def forward(self, x):
        x = self.layer_norm(x)
        x = self.w_2(F.gelu(self.w_1(x)))
        x = self.dropout(x)
        return x
