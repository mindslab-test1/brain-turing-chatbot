import torch.nn as nn
import pytorch_lightning as pl
import torch.nn.functional as F
import torch
import math
import numpy as np

from .Block import Encoder
from .Embedding import (
    UntiedAbsolutePositionEmbedding,
    Embedding,
    T5RelativePositionEmbedding,
    MLMHead,
)
from pprint import pprint
import time


class UniLM(pl.LightningModule):
    def __init__(self, args, mcst):
        super().__init__()
        self.save_hyperparameters(args)
        self.pad_id = mcst.s.pad_id()

        self.embed = Embedding(args=self.hparams, vocab=mcst.s)
        self.encoder = Encoder(args=self.hparams)
        self.tokenizer = mcst

        if self.hparams.absolute_position_encoding == 'untied_absolute_learnable':
            print('Using untied_absolute_learnable for position encoding.')
            self.untied_absolute_position_embedding = UntiedAbsolutePositionEmbedding(
                max_position_len=self.hparams.max_token_len_per_segment,
                hidden_size=self.hparams.hidden_size,
                num_attention_heads=self.hparams.multi_heads,
                attention_scale_factor=self.hparams.attention_scale_factor,
            )

        if self.hparams.use_t5_relative_position_encoding:
            print('Using t5_relative_position_encoding.')
            self.t5_relative_position_embedding = T5RelativePositionEmbedding(
                num_relative_position_bins=self.hparams.t5_relative_position_encoding_num_buckets,
                num_attention_heads=self.hparams.multi_heads
            )

        if self.hparams.decouple_mlm_head:
            self.mlm_head = MLMHead(hidden_size=self.hparams.hidden_size, embed_size=self.hparams.embed_size,
                                    vocab_size=mcst.s.vocab_size())

        for p in self.parameters():
            if p.dim() > 1:
                nn.init.xavier_uniform_(p)

    def forward(
            self,
            token_indices,
            segment_indices,
            mask=None,
            absolute_position_indices_per_segment=None,
            relative_position_indices=None,
            reserved_token_absolute_position_logit_mask=None,
            inference=True
    ):
        token_indices = self.embed(token_indices=token_indices, segment_indices=segment_indices)

        position_logits = None
        if self.hparams.absolute_position_encoding == 'untied_absolute_learnable':
            absolute_position_logits = self.untied_absolute_position_embedding(
                position_indices=absolute_position_indices_per_segment,
                untied_reserved_token_masks=reserved_token_absolute_position_logit_mask,
            )
            position_logits = absolute_position_logits

        if relative_position_indices is not None:
            relative_position_logits = self.t5_relative_position_embedding(relative_position_indices)
            position_logits = position_logits + relative_position_logits if position_logits is not None else relative_position_logits

        hidden_states, _ = self.encoder(token_indices, mask.unsqueeze(1), position_logits, inference)

        if self.hparams.decouple_mlm_head:
            logits = self.mlm_head(hidden_states)
        else:
            # (B, seq_len, h) * (h, emb) -> (B, seq_len, emb)
            embeddings = torch.matmul(hidden_states, self.embed.token.linear.weight)
            # (B, seq_len, emb) * (emb, vocab) -> (B, seq_len, vocab)
            logits = torch.matmul(embeddings, self.embed.token.embed.weight.t())

        return logits, token_indices

    def training_step(self, batch, batch_idx):
        if self.hparams.debug:
            width = 1000
            for key, value in batch.items():
                if torch.is_tensor(value):
                    print(key, value.size())
                    pprint(value.tolist(), width=width)
                    print()

        out, _ = self(
            token_indices=batch['input_ids'],
            segment_indices=batch['segment_indices'],
            absolute_position_indices_per_segment=batch['absolute_position_indices_per_segment'],
            mask=batch['cloze_attention'],
            relative_position_indices=
            batch['relative_position_indices'] if self.hparams.use_t5_relative_position_encoding else None,
            # TODO: There may not be a mask even when TUPE is ueed.
            reserved_token_absolute_position_logit_mask=
            batch[
                'reserved_token_absolute_position_logit_mask'] if self.hparams.absolute_position_encoding == 'untied_absolute_learnable' else None,
            inference=False
        )
        loss = F.cross_entropy(out.permute(0, 2, 1), batch['y'], ignore_index=self.pad_id)
        self.log('train_loss', loss)
        self.log(
            'train_acc',
            calculate_accuracy(logits=out, targets=batch['y'], padding_idx=self.pad_id),
        )
        return loss

    def validation_step(self, batch, batch_idx):
        if self.hparams.debug:
            width = 1000
            for key, value in batch.items():
                if torch.is_tensor(value):
                    print(key, value.size())
                    pprint(value.tolist(), width=width)
                    print()

        out, _ = self(
            token_indices=batch['input_ids'],
            segment_indices=batch['segment_indices'],
            absolute_position_indices_per_segment=batch['absolute_position_indices_per_segment'],
            mask=batch['cloze_attention'],
            relative_position_indices=
            batch['relative_position_indices'] if self.hparams.use_t5_relative_position_encoding else None,
            reserved_token_absolute_position_logit_mask=
            batch[
                'reserved_token_absolute_position_logit_mask'] if self.hparams.absolute_position_encoding == 'untied_absolute_learnable' else None,
            inference=False
        )
        loss = F.cross_entropy(out.permute(0, 2, 1), batch['y'], ignore_index=self.pad_id)

        if batch_idx == 0:
            mlm_tgt = batch['y'][-1:, ...].tolist()[-1]
            # mlm_tgt = [self.tokenizer.s.IdToPiece(t) for t in mlm_tgt]

            mlm_pred = np.argmax(out[-1, ...].detach().cpu().numpy(), axis=-1).tolist()
            # mlm_pred = [self.tokenizer.s.IdToPiece(p) for p in mlm_pred]
            for t, p in zip(mlm_tgt, mlm_pred):
                if t != self.pad_id:
                    t = self.tokenizer.s.IdToPiece(t)
                    p = self.tokenizer.s.IdToPiece(p)
                    print(f'mlm_target:{t}\tmlm_pred:{p}')

        self.log('val_loss', loss)
        self.log('val_acc', calculate_accuracy(logits=out, targets=batch['y'], padding_idx=self.pad_id))
        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(
            self.parameters(),
            lr=self.hparams.learning_rate,
            betas=(self.hparams.adam_beta_1, self.hparams.adam_beta_2),
            weight_decay=self.hparams.adam_weight_decay,
        )

        if self.hparams.schedule_scheme == 'none':
            return optimizer

        assert self.hparams.max_steps > self.hparams.warmup_steps > 0, 'max_steps > warmup_steps > 0 conditon is not fulfilled'

        if self.hparams.schedule_scheme == 'linear':
            def linear_warmup(current_step):
                if current_step < self.hparams.warmup_steps:
                    return current_step / self.hparams.warmup_steps
                else:
                    return (self.hparams.max_steps - current_step) / (
                                self.hparams.max_steps - self.hparams.warmup_steps)

            schedule_scheme = linear_warmup
        elif self.hparams.schedule_scheme == 'hard_cosine':
            def cosine_with_hard_restart_warmup(current_step):
                if current_step < self.hparams.warmup_steps:
                    return float(current_step) / float(max(1, self.hparams.warmup_steps))
                progress = float(current_step - self.hparams.warmup_steps) / float(
                    max(1, self.hparams.max_steps - self.hparams.warmup_steps))
                if progress >= 1.0:
                    return 0.0
                return max(0.0, 0.5 * (1.0 + math.cos(math.pi * ((float(self.hparams.num_cycles) * progress) % 1.0))))

            schedule_scheme = cosine_with_hard_restart_warmup
        else:
            raise Exception("check 'scheduler_scheme' argument . supported schemes :  [linear | hard_cosine | none] ")

        warmup_scheduler = torch.optim.lr_scheduler.LambdaLR(
            optimizer=optimizer,
            lr_lambda=schedule_scheme,
            last_epoch=-1,
        )

        schedulers = [
            {
                'scheduler': warmup_scheduler,
                'interval': 'step',
                'frequency': 1,
                'strict': True,
                'name': 'linear_warmup_lr'
            }
        ]
        return [optimizer], schedulers


# TODO: Set default pad_id to None and modify the code accordingly.
def calculate_accuracy(logits, targets, padding_idx):
    '''
    :param logits: A tensor of shape (batch_size, seq_len, vocab_size) containing the logit values of each vocabularay indices.
    :type logits: torch.FloatTensor
    :param targets: A tensor of shape (batch_size, seq_len) containing the target indices of the sequence.
    :type targets: torch.LongTensor
    :param pad_id: The padding index in the targets tensor
    :type pad_id: int
    :return: A one-dimensional accuracy tensor calculated by comparing the logits and the targets tensors.
    :rtype: torch.FloatTensor
    '''
    # Get the top-1 logit indices.
    # TODO: Use argmax.
    predictions = torch.max(logits, dim=-1).indices
    # Reset the padding index to -1.
    targets = torch.where(
        targets == padding_idx,
        torch.tensor(-1, dtype=torch.long, device=targets.device),
        targets
    )
    # Calculate the number of correct predictions.
    num_correct_predictions = torch.count_nonzero(predictions == targets, dim=None)
    # Count the number of non-padding tagets.
    num_targets = torch.count_nonzero(targets != -1, dim=None)
    # Calculate the accuracy by dividing the number of correct predictions by the number of non-padding targets.
    accuracy = torch.true_divide(num_correct_predictions, num_targets)
    return accuracy
