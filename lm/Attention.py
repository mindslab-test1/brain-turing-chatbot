import torch
import torch.nn as nn
import torch.nn.functional as F


class Attention(nn.Module):
    def __init__(self, args):
        super().__init__()
        self.scaled = (args.attention_scale_factor * args.hidden_size // args.multi_heads) ** .5
        self.dropout = nn.Dropout(p=args.dropout)

    def forward(self, q, k, v, attention_weight_bias=None, mask=None):
        attention = torch.matmul(q, k.transpose(2, 3)) / self.scaled
        mask_value = torch.finfo(attention.dtype).min

        if attention_weight_bias is not None:
            attention += attention_weight_bias

        if mask is not None:
            attention.masked_fill_(~mask, mask_value)
        

        attention = F.softmax(attention, dim=-1)

        out = self.dropout(attention)
        out = torch.matmul(out, v)

        return out, attention
