from pathlib import Path
from tqdm import tqdm
import random
from dataclasses import dataclass
from typing import List, Dict

import torch
from torch.utils.data import Dataset

from .task_Mask import Mask
from .mlm_masking import UniLMMaskingScheme
from lm.Embedding import T5RelativePositionEmbedding, create_reserved_token_position_logit_reset_matrix


class NoNSPUniLMDataset(Dataset):
    def __init__(self, cfg, run, tokenizer, seed): 
        """
        run(str): now state. [train/val/test]
        """
        self.cfg = cfg
        self.corpus_path = Path(self.cfg.corpusdir, run)
        self.tokenizer = tokenizer
        self.pad_id = self.tokenizer.s.PieceToId('<pad>')
        self.global_id = self.tokenizer.s.PieceToId('<glb>')
        self.max_seq_len = self.cfg.max_seq_len

        self.ums = UniLMMaskingScheme(
                self.cfg,
                self.tokenizer,
                self.pad_id,
                tokenizer.s.PieceToId('<mask>'),
                tokenizer.s.PieceToId('<eos>'),
                self.global_id)
        
        self.Mask = Mask()
        self.masking_schemes = ['unidirection', 'bidirection', 'seq_to_seq']
        
        self.file_index = list(self.corpus_path.glob('*'))
        self.size = len(self.file_index)

        self.num_reserved_token_reset_values = 0
        if self.cfg.use_t5_relative_position_encoding or self.cfg.absolute_position_encoding == 'untied_absolute_learnable':
            offsets = []
            # TODO: Use enum or something else, anything but the raw string representations.
            for reset_config in ['global_to_internal_offset', 'global_to_external_global_offset', 'normal_to_internal_global_offset', 'normal_to_external_global_offset']:
                if getattr(self.cfg, reset_config, None) is not None:
                    offsets.append(getattr(self.cfg, reset_config))
            # The number of unique offset values must be equal to the maximum offset value.
            assert max(offsets) == len(set(offsets))
            # The smallest offset value must be one if there is at least one offset value that is not None.
            if len(offsets) > 0:
                assert min(offsets) == 1
            self.num_reserved_token_reset_values = len(set(offsets))

        # TODO: Check for edge case errors and raise appropriate exceptions.
        if self.cfg.use_t5_relative_position_encoding:
            self.t5_relative_position_matrix = T5RelativePositionEmbedding.create_t5_relative_position_encoding_matrix(
                sequence_len=cfg.max_seq_len,
                # Subtract the number of reserved token reset values.
                num_buckets=cfg.t5_relative_position_encoding_num_buckets - self.num_reserved_token_reset_values,
                max_distance=cfg.t5_relative_position_encoding_max_distance, 
            )
            self.global_token_reset_val_max = self.cfg.t5_relative_position_encoding_num_buckets
        else:
            self.global_token_reset_val_max = 5

    # TODO: Consider writing TUPE, UniLM related tokenizer(s) and delegate most of the index processing functionality to the tokenizer(s) class.
    def __getitem__(self, i):
        filename = self.corpus_path / f'{self.file_index[i]}'
        block, line, ids_to_tokens = torch.load(str(filename))
        block = torch.tensor(block)
        assert len(block) == self.max_seq_len

        task_mask = self._mask_block(block)
        sentence, target = self.ums.mask(block)

        seqlen = len(block[block!=self.pad_id])

        segment_indices = []
        segment_index = 0
        max_segment_index = 0

        global_token_indices = []

        absolute_position_indices_per_segment = list(range(self.num_reserved_token_reset_values)) if self.cfg.absolute_position_encoding == 'untied_absolute_learnable' else []
        absolute_position_index_per_segment = self.num_reserved_token_reset_values if self.cfg.absolute_position_encoding == 'untied_absolute_learnable' else 0

        # TODO: Make autoregressive segments' indices start from 0.
        # TODO: Print warning if either of the segment length or the absolute position length exceeds the maximum configured.
        for i, token_id in enumerate(block):
            if token_id == self.pad_id:
                last_tokens = [0] * (self.max_seq_len - i)
                # Pad tokens do not end up to be zero but it really does not matter since they get masked anyway.
                segment_indices.extend(last_tokens)
                absolute_position_indices_per_segment.extend(last_tokens)
                break

            if segment_index > max_segment_index:
                max_segment_index = segment_index
            segment_indices.append(segment_index)

            absolute_position_indices_per_segment.append(absolute_position_index_per_segment)
            absolute_position_index_per_segment += 1

            if token_id == self.global_id:
                segment_index += 1
                absolute_position_index_per_segment = self.num_reserved_token_reset_values
                global_token_indices.append(i)

        segment_indices = [- (segment_index - max_segment_index) for segment_index in segment_indices] 

        # TODO: Please, oh, please, do take care of these if statements...
        
        # TODO: Figure out why num_buckets // 2 is never used in certain cases (if not always).
        if self.cfg.use_t5_relative_position_encoding or self.cfg.absolute_position_encoding == 'untied_absolute_learnable':
            reserved_token_position_logit_reset_matrix = create_reserved_token_position_logit_reset_matrix(
                global_token_indices=global_token_indices,
                sequence_len=self.max_seq_len,
                global_to_internal=
                    self.global_token_reset_val_max - self.cfg.global_to_internal_offset if self.cfg.global_to_internal_offset is not None else None,
                global_to_external_global=
                    self.global_token_reset_val_max - self.cfg.global_to_external_global_offset if self.cfg.global_to_external_global_offset is not None else None,
                normal_to_internal_global=
                    self.global_token_reset_val_max - self.cfg.normal_to_internal_global_offset if self.cfg.normal_to_internal_global_offset is not None else None,
                normal_to_external_global=
                    self.global_token_reset_val_max - self.cfg.normal_to_external_global_offset if self.cfg.normal_to_external_global_offset is not None else None,
                fill_empty_space_with=-1,
            ) 

        if self.cfg.use_t5_relative_position_encoding:
            # .detach().clone() may not be necessary.
            relative_position_indices = self.t5_relative_position_matrix[:len(sentence), :len(sentence)]
            # reset reserved token related indices.
            relative_position_indices = relative_position_indices.where(
                reserved_token_position_logit_reset_matrix == -1,
                reserved_token_position_logit_reset_matrix,
            )

        if self.cfg.absolute_position_encoding == 'untied_absolute_learnable':
            reserved_token_absolute_position_logit_mask = [
                reserved_token_position_logit_reset_matrix != (self.global_token_reset_val_max - getattr(self.cfg, reset_config))
                for reset_config in ['global_to_internal_offset', 'global_to_external_global_offset', 'normal_to_internal_global_offset', 'normal_to_external_global_offset']
                if getattr(self.cfg, reset_config, None) is not None
            ]
            if len(reserved_token_absolute_position_logit_mask) >= 1:
                # (seq_len, seq_len, num_reserved).
                reserved_token_absolute_position_logit_mask = torch.stack(reserved_token_absolute_position_logit_mask, -1)
            else:
                reserved_token_absolute_position_logit_mask = None

        if self.cfg.full_bidirectional:
            task_mask = torch.ones((self.max_seq_len, self.max_seq_len), dtype=torch.bool)
            task_mask[: seqlen:] = False
            task_mask[seqlen:, :] = False

        if self.cfg.full_absolute_position_encoding:
            absolute_position_indices_per_segment = list(range(self.max_seq_len + self.num_reserved_token_reset_values))

        return_dict = {
            # 'tokens': ids_to_tokens,
            'input_ids': torch.tensor(sentence),
            'segment_indices': torch.tensor(segment_indices),
            'absolute_position_indices_per_segment': torch.tensor(absolute_position_indices_per_segment),
            'y': torch.tensor(target),
            'cloze_attention': task_mask,
        }
        if self.cfg.absolute_position_encoding == 'untied_absolute_learnable' and len(reserved_token_absolute_position_logit_mask) >= 1:
            return_dict['reserved_token_absolute_position_logit_mask'] = reserved_token_absolute_position_logit_mask

        if self.cfg.use_t5_relative_position_encoding:
            return_dict['relative_position_indices'] = relative_position_indices

        return return_dict

    def __len__(self):
        return self.size

    def _mask_block(self, sentence):
        MASK_SCHEME = {
            'unidirection':0,
            'bidirection':len(sentence),
            'seq_to_seq':None}
        masking_scheme = random.choice(self.masking_schemes)
        line = MASK_SCHEME[masking_scheme]
        block = self.Mask.mask(sentence, self.global_id, self.pad_id, line=line) # uni, bi, seq-to-seq
        return block



import argparse

from vocab.Change import Change
from vocab.MCSTokenizer import MCSTokenizer
import sentencepiece as spm
from konlpy.tag import Mecab
import torch
from tqdm import tqdm
from pathlib import Path

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--mask_ratio', default='0.15', type=float)
    parser.add_argument('--corpusdir', default='/data/engines/lm/UniLM/corpus/mp_corpus/', type=str)
    parser.add_argument('--max_seq_len', default=2048, type=int)
    parser.add_argument('--stride', default=4)
    parser.add_argument('--c', default=1)
    parser.add_argument('--w', default=1)
    parser.add_argument('--r', default=2)

    cfg = parser.parse_args()
    
    sp = spm.SentencePieceProcessor(model_file='/workspace/brain-turing-chatbot/vocab/mecab_change_50K_uni.model')
    tokenizer = MCSTokenizer(tagger=Mecab(), change=Change(), sp=sp)
    
    train_dataset = NoNSPUniLMDataset(cfg, 'test', tokenizer, 34)
    for idx in tqdm(range(10), desc='testing...'):
        td1 = train_dataset[0]
        assert len(td1.x['inter_position_ids'])== len(td1.x['token_type_ids']) == len(td1.x['input_ids'])
    print('inputs', td1.x)
    print('y', td1.y)
    print('cloze attention', td1.cloze_attention.int())
    '''
    print('-' * 50)
    zz = td1.x
    tt = td1.tokens
    for x,y,z,t in zip(zz['inter_position_ids'], zz['token_type_ids'], zz['input_ids'], td1.tokens):
        print(y,x,z,t)
        if x==y==0 and z==1:
            break
    '''
