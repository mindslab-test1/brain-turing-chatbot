# ==========================
# indexed_dataset.py
# Load corpus files as input and binarizes it in memory at instantiation.
# jjl
# ref.
# ==========================
import os
import re
import struct
import pickle
from pathlib import Path
from collections import defaultdict, deque
import itertools
from tqdm import tqdm
from typing import List, Dict, Union
import copy
import emoji
from soynlp.normalizer import repeat_normalize
from kss import split_sentences

import numpy as np

import torch
import logging

# https://ko-nlp.github.io/Korpora/ko-docs/introduction/quicktour.html
logger = logging.getLogger(__name__)


def index_file_path(prefix_path):
    return prefix_path + '.idx'


def data_file_path(prefix_path):
    return prefix_path + '.bin'


def isHangeul(one_character):
    """글자 하나를 입력받아, 이것이 조합된 한글 혹은 영어인지 판단한다."""
    return 0xAC00 <= ord(one_character[:1]) <= 0xD7A3 or 65 <= ord(one_character[:1]) <= 122 or ord(
        one_character[:1]) == 32


def ispass(one_character):
    """ 자주 사용되는 특문도 봐주기로 함"""
    return ord('!') <= ord(one_character[:1]) < ord('0') or ord('9') < ord(one_character[:1]) <= ord('@')


emojis = ''.join(emoji.UNICODE_EMOJI.keys())
pattern = re.compile(f'[^ .,?!/@$%~％·∼()\x00-\x7Fㄱ-힣{emojis}]+')  # \x00-\x7F : 아스키문자
url_pattern = re.compile(
    r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)')


def clean(x):
    x = pattern.sub(' ', x)
    x = url_pattern.sub('', x)
    x = x.strip()
    x = repeat_normalize(x, num_repeats=2)
    return x


def check_utterance(utterance_token_ids, utterance_str, speaker_name=None):
    # The author is a known bot.
    if speaker_name is not None:
        if 'bot' in speaker_name:
            # maybe use 're' library..
            return False

    # The number of BPE tokens is more than 128 or less than 2.
    if not 2 < len(utterance_token_ids) < 128:
        return False

    # Any word has more than 30 characters or the message has more than 1024 characters.
    if len(utterance_str) > 1024:
        return False
    for u in utterance_str.split(' '):
        if len(u) > 30:
            return False
    # The message contains URL.
    crawl = "(http|https):\/\/([\xA1-\xFEa-z0-9_\-]+\.[\xA1-\xFEa-z0-9:;&#@=_~%\/\?\.\,\+\-]+)"
    if re.findall(crawl, utterance_str):
        return False
    # if utterance_str contains offensive words

    return True


def check_message(message):
    pass


def list2np(X, padding):
    # 최대 sentence 길이로 pad 한다.
    # 그러기 위해 shape 를 알아야 한다 그ㅓㄹ나 list 에서는 shape 를 지원하지 않으므로 직접 계산해야 한다.
    # shape를 계산한 이후, 해당 shape 에 맞춰 패딩을 해주어야한다.
    # 우선 padding id 로 채워진 shape 크기의 np 를 만든 후, 각 sentence를 돌며 pad가 아닌 부분을 대체해줘야한다.
    # shape 의 길이는 곧 차원이 된다. 문장 하나ㅏㅁㄴ 들어올 때, 문장 여러개가 들어올 때(하나의 배치), 대화 여러개가 들어올 때(dist 인듯?)를 구현해야 한다!
    # 라고 plato 에서 코드를 짜두었다.

    def shape(x, max_seq_len):
        pass

    samples_num = len(X)

    pass


class BlockedDataset:
    """Takes a text file as input and blocks of max_seq_len tokens it in memory at instantiation.
    Original lines are also kept in memory"""

    def __init__(self,
                 cfg,
                 tokenizer,
                 mecab=None,
                 cls_token=None,
                 cls_id=None,
                 sep_token=None,
                 sep_id=None,
                 pad_token=None,
                 pad_id=None,
                 global_token=None,
                 global_id=None):
        self.cfg = cfg
        self.dest_path = Path(self.cfg.destdir)

        self.max_token_num = cfg.max_seq_len - 1
        self.max_ctx_turn = cfg.max_ctx_turn if cfg.max_ctx_turn is not None else 10000
        self.uttr_num = 0
        self._doc_ids = []
        self._doc_lines = []
        self._doc_tokens = []
        self._doc_embeds = {'speaker_embedding': [],
                            'turn_embedding': [],
                            'position_embedding': []}

        self.tokenizer = tokenizer
        self.mecab = mecab

        self.cls_token = cls_token
        self.cls_id = cls_id
        self.sep_token = sep_token
        self.sep_id = sep_id
        self.pad_token = pad_token
        self.pad_id = pad_id
        self.global_token = global_token
        self.global_id = global_id
        self.average_sent_num = []

        self.idx = 0

    def add_eos_to_sentences(self, sentences):
        """
        - sentences : List[str]
        - eos_id : int
        - eos_token: str
        return)
        - ids_outputs: List[List[str]] 문장 단위로 list로 묶인 token ids
        - tokens_outputs: List[List[int]] 문장 단위로 list로 묶인 token
        - total_token_length: int : 총 token 개수
        """
        ids_outputs = []
        tokens_outputs = []
        for sentence in sentences:
            if len(ids_outputs) != 0:
                ids_outputs.extend([self.sep_id, self.global_id])
                tokens_outputs.extend([self.sep_token, self.global_token])

            ids = self.tokenizer.EncodeAsIds(sentence)
            tokens = self.tokenizer.EncodeAsPieces(sentence)
            ids_outputs.extend(ids)
            tokens_outputs.extend(tokens)

        assert len(ids_outputs) == len(tokens_outputs), 'check tokenizer results'
        return ids_outputs, tokens_outputs

    def convert_sentence_representation_format(self, ids, tokens):
        """

        :param ids:
        :param tokens:
        :return:
        """
        ids += [self.sep_id, self.global_id]
        tokens += [self.sep_token, self.global_token]
        return ids, tokens

    def get_average_sent_num(self):
        """
        한 block당 sentence 개수의 평균을 계산하는 메타함수
        :return:
        """
        try:
            return sum(self.average_sent_num) / len(self.average_sent_num)
        except ZeroDivisionError:
            return 0

    @staticmethod
    def split_paragraph(self, doc: List) -> List[Dict[str, Union[str, int]]]:
        """
        문서 (혹은 담화)를 받아서, 문장 (혹은 발화) 리스트를 반환한다.
        :param doc: paragraph list.
        :return: List[
                    Dict [
                        'text' : text,
                        'speaker': speaker id. str if utterance, None if txt,
                        'turn': turn id. int if utterance, None if txt
                        ]
                    ]
        """
        raise NotImplementedError

    @staticmethod
    def save_block(self):
        raise NotImplementedError

    def _fill_blocks(self, ids, paragraph, tokens, speaker_id):
        """

        :param ids:
        :param paragraph:
        :param tokens:
        :param speaker_id:
        :return:
        """
        # print(f'fill the utterance in block[{self.uttr_num}], {len(self._doc_ids)} {len(self._doc_embeds["speaker_embedding"])}')
        if speaker_id is not None:
            self._doc_ids.append(ids)
            self._doc_tokens.append(tokens)
        else:
            self._doc_ids.extend(ids)
            self._doc_tokens.extend(tokens)
        self._doc_lines.append(paragraph)

        if speaker_id is not None:
            self._doc_embeds['speaker_embedding'].append(speaker_id)
            self._doc_embeds['turn_embedding'].append(len(self._doc_embeds['turn_embedding']))
            self._doc_embeds['position_embedding'].append(range(len(ids)))
            assert len(self._doc_embeds['speaker_embedding']) == len(self._doc_embeds['turn_embedding'])
            assert len(self._doc_ids) == len(self._doc_embeds['speaker_embedding']), \
                f"{self._doc_ids} == {self._doc_embeds['speaker_embedding']}"
        self.uttr_num += 1

    def _reset_blocks(self, Ids=[], Paragraph=None, Tokens=[], Speaker_id=None):
        """

        :param Ids:
        :param Paragraph:
        :param Tokens:
        :param Speaker_id:
        :return:
        """
        # print(Ids)
        self._doc_ids = [Ids] if Ids != [] else []
        self._doc_lines = Paragraph if Paragraph != None else []
        self._doc_tokens = [Tokens] if Tokens != [] else []
        self._doc_embeds = {'speaker_embedding': [Speaker_id] if Speaker_id != None else [],
                            'turn_embedding': [1] if Speaker_id != None else [],
                            'position_embedding': []}
        if Ids != []:
            self._doc_embeds['position_embedding'].append(range(len(Ids)))

    def save_blocks(self, corpora, rank, mode):
        """
        작업할 corpora와 process rank 만 받아온다.
        block을 저장할 때 형식은 "{04rank}_{06idx}"
        """
        self.rank = rank

        self.dest_path /= mode
        name = mode
        self.passed_paragraph = 0
        self.done_paragraph = 0

        remain_num = self.max_token_num

        for d in tqdm(range(len(corpora)), desc=f'[mp:{self.rank}] {name}_tokenizing..'):
            doc = corpora[d]
            if len(doc) < self.cfg.threshold:
                continue
            paragraphs = self.split_paragraph(doc)

            for j in range(len(paragraphs)):
                # TODO 현재 paragraphs : list(dict(), dict() ... ) 인데, dict(text:list, speaker:list, turn:list ) 로 하는 것이 더 낫나..? 흠
                paragraph = paragraphs[j]['text']
                speaker_id = paragraphs[j].get('speaker', None)
                paragraph = clean(str(paragraph))

                if len(paragraph) == 0:
                    continue
                ids = self.tokenizer.EncodeAsIds(paragraph)
                tokens = self.tokenizer.EncodeAsPieces(paragraph)

                # for GT
                if (self.cfg.filter_utterance) and \
                        (not check_utterance(ids, paragraph, speaker_name=speaker_id)):
                    # True: keep going
                    # False: filtered utterance. can't use.
                    # print(f'[system: utterance filtered] ')
                    if self._doc_ids.count(self.global_id) > 10 or self.uttr_num > 8:
                        self.save_block()
                    self._reset_blocks()
                    self.uttr_num = 0
                    remain_num = self.max_token_num
                    continue

                if len(ids) >= self.max_token_num:
                    self.passed_paragraph += 1
                    if 'plato' in self.cfg.data_type:
                        # print('# 반복됨!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                        if self._doc_ids.count(self.global_id) > 10 or self.uttr_num > 8:
                            self.save_block()
                        self._reset_blocks()
                        self.uttr_num = 0
                        remain_num = self.max_token_num
                    continue

                if self.uttr_num > self.max_ctx_turn:
                    pushed_uttr_num = self.save_block(len(ids))
                    assert pushed_uttr_num <= 1024

                    if pushed_uttr_num == -1:  # txt file
                        # print('im in 286 line, text file processing! ')
                        self._reset_blocks()
                        self.uttr_num = 0
                        remain_num = self.max_token_num
                    else:  # json file
                        # print('im in json file processing! ')
                        assert remain_num + pushed_uttr_num + len(
                            list(itertools.chain(*self._doc_ids))) == self.max_token_num
                        remain_num += pushed_uttr_num
                    continue
                if (remain_num - 2 - len(ids)) > 0 and (
                        self.uttr_num < self.max_ctx_turn):  # check save block 함수를 만드는게 좋을까?
                    ids, tokens = self.convert_sentence_representation_format(ids, tokens)

                    self._fill_blocks(ids, paragraph, tokens, speaker_id)
                    remain_num -= len(ids)
                    # print('just fill block! ')
                    # print(f"{remain_num} + {len(list(itertools.chain(*self._doc_ids)))} != {self.max_token_num}\
                    #    ({remain_num + len(list(itertools.chain(*self._doc_ids))) == self.max_token_num }) ")
                    # print()
                    assert remain_num <= 1024, f'[{d} - {j}] pushed ghost utter!! {remain_num} {self._doc_ids}'
                    assert remain_num > 0, remain_num
                else:
                    if remain_num > self.cfg.threshold and not self.cfg.keep_sentence:
                        # print(f' | remain_num{remain_num} > {self.cfg.threshold}')
                        # if we train PLATO, last utterance is important!
                        ids, tokens = self.convert_sentence_representation_format(ids[:remain_num - 2],
                                                                                  tokens[:remain_num - 2])
                        self._fill_blocks(ids, paragraph, tokens, speaker_id)
                        self.save_block()
                        # print(f' | save_block! uttr_num: [{self.uttr_num}]')
                        self._reset_blocks()
                        # print(f' | can reset_blocks? : {len(self._doc_ids)}')
                        self.uttr_num = 0
                        remain_num = self.max_token_num
                    else:
                        # print(f' | remain_num{remain_num} <= {self.cfg.threshold}')
                        # print(f' | save_block??! uttr_num: [{self.uttr_num}]')
                        ids, tokens = self.convert_sentence_representation_format(ids, tokens)
                        pushed_uttr_num = self.save_block(len(ids))
                        remain_num += pushed_uttr_num

                        if pushed_uttr_num != -1:
                            # print('PLATO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                            self._fill_blocks(ids, paragraph, tokens, speaker_id)
                            remain_num -= len(ids)
                            # print('no place remain!! push block')
                            # print(f"{remain_num} + {len(list(itertools.chain(*self._doc_ids)))} != {self.max_token_num}\
                            #    ({remain_num + len(list(itertools.chain(*self._doc_ids))) == self.max_token_num }) ")
                            # print()
                        else:
                            # print('no plato?')
                            self._reset_blocks(Ids=ids, Paragraph=paragraph, Tokens=tokens, Speaker_id=speaker_id)
                            self.uttr_num = 1
                            # print(f' | reset blocks? {len(ids)} => {len(self._doc_ids)}')
                            remain_num = self.max_token_num - len(ids)

                self.done_paragraph += 1
                # print(j, remain_num, len(self._doc_ids))
                # print(f"[check! : why remain_num exploded ??? {remain_num}] ({self.uttr_num})")

            if self.cfg.separate_paragraph:
                # print(f"[system: don't merge paragraphs. one paragraph finished]")
                if ('plato' in self.cfg.data_type and len(self._doc_ids) > 10) or \
                        (self.cfg.data_type == 'pretrain' and len(self._doc_ids) > 0):
                    self.save_block()
                self._reset_blocks()
                remain_num = self.max_token_num
                self.uttr_num = 0

        if len(self._doc_ids) > 0:
            if ('plato' not in self.cfg.data_type) or \
                    ('plato' in self.cfg.data_type and len(self._doc_ids) > 10):
                self.save_block()
            self._reset_blocks()
            remain_num = self.max_token_num
            self.uttr_num = 0


class BlockedRawPlaintextDataset(BlockedDataset):
    """Takes a text file as input and blocks of max_seq_len tokens it in memory at instantiation.
    Original lines are also kept in memory"""

    def __init__(self,
                 cfg,
                 tokenizer,
                 mecab=None,
                 cls_token=None,
                 cls_id=None,
                 sep_token=None,
                 sep_id=None,
                 pad_token=None,
                 pad_id=None,
                 global_token=None,
                 global_id=None):
        super().__init__(cfg, tokenizer, mecab, cls_token, cls_id, sep_token, sep_id, pad_token, pad_id, global_token,
                         global_id)

    def split_paragraph(self, doc):
        paragraphs = split_sentences(' '.join(doc.split('\n')))
        res = []
        for p in paragraphs:
            res.append({'text': p})
        return res

    def save_block(self, utterance_num=None):
        block = self._doc_ids
        line = self._doc_lines
        tokens = self._doc_tokens

        b = [self.cls_id] + block + [self.pad_id] * (
                self.max_token_num - len(block))

        if isinstance(line, list):
            l = f' '.join(line)
        elif isinstance(line, str):
            l = line
        i = self.tokenizer.convert_ids_to_tokens(block) if tokens is None else tokens
        i = [self.cls_token] + i
        assert len(b) == self.max_token_num + 1, f'check token length : {len(b)} {b}'

        filename = self.dest_path / f'{self.rank}_{self.idx}'
        torch.save([b, l, i], filename)
        self.idx += 1
        self.average_sent_num.append(b.count(self.global_id))
        return -1


class BlockedRawUtteranceDataset(BlockedDataset):
    def __init__(self,
                 cfg,
                 tokenizer,
                 mecab=None,
                 cls_token=None,
                 cls_id=None,
                 sep_token=None,
                 sep_id=None,
                 pad_token=None,
                 pad_id=None,
                 global_token=None,
                 global_id=None,
                 bos_token=None,
                 bos_id=None):
        super().__init__(cfg, tokenizer, mecab, cls_token, cls_id, sep_token, sep_id, pad_token, pad_id, global_token,
                         global_id)

        self._doc_ids = deque([])  # <= cfg.max_seq_len tokens
        self._doc_lines = deque([])
        self._doc_tokens = deque([])
        self._doc_embeds = {'speaker_embedding': deque([]),
                            'turn_embedding': deque([]),
                            'position_embedding': deque([])}
        self.bos_token = bos_token
        self.bos_id = bos_id
        self.min_uttr_num = cfg.min_uttr_num
        self.max_uttr_num = cfg.max_uttr_num
        self.min_ctx_turn = cfg.min_ctx_turn
        self.max_ctx_turn = cfg.max_ctx_turn - 1  # subtaract reply turn
        self.utterance_level = cfg.utterance_level if cfg.utterance_level else 'sentence'

    def split_paragraph(self, dialog):
        """

        :param dialog:
        :return:
        """
        cur_utterance_id = 0
        res = []
        for utterance in dialog:
            cur_speaker = utterance['speaker']
            if len(str(utterance['utterance'])) > 1024 and self.utterance_level == 'sentence':
                paragraphs = split_sentences(utterance['utterance'])
                for p in paragraphs:
                    res.append({
                        'text': p,
                        'speaker': cur_speaker,
                        'turn': cur_utterance_id
                    })

                    cur_utterance_id += 1
            else:
                res.append({
                    'text': utterance['utterance'],
                    'speaker': cur_speaker,
                    'turn': cur_utterance_id
                })

                cur_utterance_id += 1

        return res

    def _reset_blocks(self, Ids=[], Paragraph=None, Tokens=[], Speaker_id=None):
        """

        :param Ids:
        :param Paragraph:
        :param Tokens:
        :param Speaker_id:
        :return:
        """
        # print(Ids)
        self._doc_ids = deque([Ids]) if Ids != [] else deque([])
        self._doc_lines = deque(Paragraph) if Paragraph != None else deque([])
        self._doc_tokens = deque([Tokens]) if Tokens != [] else deque([])
        self._doc_embeds = {'speaker_embedding': deque([Speaker_id]) if Speaker_id != None else deque([]),
                            'turn_embedding': deque([1]) if Speaker_id != None else deque([]),
                            'position_embedding': deque([])}
        if Ids != []:
            self._doc_embeds['position_embedding'].append(range(len(Ids)))

    def _convert_utterance_input_representation(self, is_split_uttr=True):
        """

        input: turn embedding (list) :
        output :
        """
        # split context and response
        blocks = list(copy.deepcopy(self._doc_ids))
        tokens = list(copy.deepcopy(self._doc_tokens))
        blocks[-1] = [self.bos_id] + blocks[-1]
        tokens[-1] = [self.bos_token] + tokens[-1]

        # convert embeds
        embeds = copy.deepcopy(self._doc_embeds)

        total_utter_num = len(blocks)
        assert total_utter_num == len(
            embeds['speaker_embedding']), f"{total_utter_num}, {len(embeds['speaker_embedding'])}"

        # convert speaker tags
        all_speaker_tags = []
        for s in embeds['speaker_embedding']:
            if s not in all_speaker_tags:
                all_speaker_tags.append(s)
        # SPEAKER_DICT = {s: chr(ord('A') + i) for i, s in enumerate(all_speaker_tags)}
        SPEAKER_DICT = {s: i for i, s in enumerate(all_speaker_tags)}

        speaker_emb = [SPEAKER_DICT[s] for s in embeds['speaker_embedding']]
        # speaker_emb = [[s] * len(ids) for ids, s in zip(blocks, speaker_emb)]
        if not is_split_uttr:
            speaker_emb = list(itertools.chain(*speaker_emb))
        embeds['speaker_embedding'] = speaker_emb

        # convert turn index response : 0 , context -1, -2 , ....
        turn_emb = embeds['turn_embedding']
        turn_embed = list(map(lambda x: x - len(turn_emb) + 1, turn_emb))
        turn_embed = [[t] * len(ids) for ids, t in zip(blocks, turn_embed)]
        if not is_split_uttr:
            turn_embed = list(itertools.chain(*turn_embed))
        embeds['turn_embedding'] = turn_embed

        pos_emb = embeds['position_embedding']
        pos_emb[-1] = range(pos_emb[-1].stop + 1)
        pos_emb = list(pos_emb)
        if not is_split_uttr:
            pos_emb = list(itertools.chain(*pos_emb))
        embeds['position_embedding'] = pos_emb

        # assert len(embeds['speaker_embedding']) == len(embeds['turn_embedding']) == len(
        #    embeds['position_embedding']), \
        #    f"{len(embeds['speaker_embedding'])} == {len(embeds['turn_embedding'])} == {len(embeds['position_embedding'])}"
        assert len(embeds['speaker_embedding']) == len(blocks), f"{len(embeds['speaker_embedding'])} != {len(blocks)} "
        return blocks, tokens, embeds

    def _push_block(self):
        """

        :return:
        """
        assert len(self._doc_ids) > 1
        pushed_uttr_num = len(self._doc_ids.popleft())
        self._doc_lines.popleft()
        self._doc_tokens.popleft()
        self._doc_embeds['speaker_embedding'].popleft()
        self._doc_embeds['turn_embedding'].pop()
        self._doc_embeds['position_embedding'].popleft()
        self.uttr_num -= 1
        return pushed_uttr_num


class BlockedGTDataset(BlockedRawUtteranceDataset):
    def save_block(self, utterance_num=None):
        """
        all blocks are appended. use chain to unpack blocks...
        :param utterance_num:
        :return:
        """
        blocks, tokens, embs = self._convert_utterance_input_representation(is_split_uttr=False)
        examples = {}

        blocks = list(itertools.chain(*blocks))
        lines = self._doc_lines
        tokens = list(itertools.chain(*tokens))
        padded_num = self.max_token_num - len(blocks) + 1

        b = [self.cls_id] + blocks + [self.pad_id] * padded_num

        '''if isinstance(line, list) and self.cfg.model_type == 'UniLM':
            l = f'{self.sep_token}{self.global_token}'.join(line)
        elif isinstance(line, str):
            l = line'''
        l = ' '.join(lines)
        i = self.tokenizer.convert_ids_to_tokens(blocks) if tokens is None else tokens
        i = [self.cls_token] + i
        assert len(b) == self.cfg.max_seq_len, f'check token length[{self.max_token_num}] : {len(b)} {b}'

        # pad all
        for k, v in embs.items():
            pre_embs = embs[k]
            embs[k] = [self.pad_id] + v + [self.pad_id] * padded_num
            assert len(b) == len(embs[k]), f'check {k} length : {len(pre_embs)} -> {len(embs[k])}. {len(b)}'
        # for idx, (ii, bb,e1, e2, e3) in enumerate(zip(i, b, *embs.values())):
        #    print(f'[{idx}] {ii}, {bb} , {e1}\t{e2}\t{e3}')
        # print('= '* 60)

        # save block
        filename = self.dest_path / f'{self.rank}_{self.idx}'
        torch.save([b, l, i, embs], filename)
        self.idx += 1
        self.average_sent_num.append(b.count(self.global_id))

        pre_tokens = len(list(itertools.chain(*self._doc_ids)))
        pushed_uttr_num = 0
        need_place = utterance_num if utterance_num is not None else len(self._doc_ids[0])
        remain_tokens = self.max_token_num - pre_tokens
        # print(f'-================================== start while {remain_tokens} {self.uttr_num} > {self.max_ctx_turn}')
        while (remain_tokens < need_place) or (self.uttr_num >= self.max_ctx_turn):
            tmp_pushed_uttr_num = self._push_block()
            pushed_uttr_num += tmp_pushed_uttr_num
            remain_tokens += tmp_pushed_uttr_num
            # print(f'[system(while in save block)] : {tmp_pushed_uttr_num} tokens pushed: remain place num({pushed_uttr_num} / {need_place})] [cur doc ids num: {len(list(itertools.chain(*self._doc_ids)))}] ')
            assert len(self._doc_ids) == len(self._doc_embeds['speaker_embedding'])
            assert len(self._doc_ids) == self.uttr_num, f'{len(self._doc_ids)} != {self.uttr_num}'

        assert pre_tokens == pushed_uttr_num + len(
            list(itertools.chain(*self._doc_ids))), f'{pre_tokens} {pushed_uttr_num} {self._doc_ids}'
        return pushed_uttr_num


class BlockedPLATODataset(BlockedRawUtteranceDataset):
    def __init__(self,
                 cfg,
                 tokenizer,
                 mecab=None,
                 cls_token=None,
                 cls_id=None,
                 sep_token=None,
                 sep_id=None,
                 pad_token=None,
                 pad_id=None,
                 global_token=None,
                 global_id=None,
                 bos_token=None,
                 bos_id=None):
        super().__init__(cfg, tokenizer, mecab, cls_token, cls_id, sep_token, sep_id, pad_token, pad_id, global_token,
                         global_id, bos_token, bos_id)

        self.max_token_num = cfg.max_seq_len - 2

    def save_block(self, utterance_num=None):
        """
        all blocks are appended. use chain to unpack blocks...
        :param utterance_num:
        :return:
        """

        # print('[system] save block!')

        blocks, tokens, embs = self._convert_utterance_input_representation()
        lines = list(copy.deepcopy(self._doc_lines))
        examples = {}
        c_blocks, r_block = blocks[:-1], [blocks[-1]]
        c_lines, r_line = lines[:-1], [lines[-1]]
        c_tokens, r_token = tokens[:-1], [tokens[-1]]

        c_embs = {k: v[:-1] for k, v in embs.items()}
        r_embs = {k: [v[-1]] for k, v in embs.items()}
        examples['src_ids'] = c_blocks
        examples['src_line'] = c_lines
        examples['src_speaker'] = c_embs['speaker_embedding']

        examples['tgt_ids'] = r_block
        examples['tgt_line'] = r_line
        examples['tgt_speaker'] = r_embs['speaker_embedding']

        blocks_num = len(list(itertools.chain(*blocks)))
        uttr_num = len(examples['src_ids']) - 1 + len(examples['tgt_ids'])
        assert blocks_num <= self.cfg.max_seq_len, f'check token length[{self.max_seq_len}] : {blocks_num}'
        assert uttr_num <= self.max_ctx_turn
        # examples['padded_num'] = self.max_token_num - blocks_num  # 필요없을듯?
        # assert blocks_num + examples['padded_num'] ==  self.max_token_num , f'{examples["padded_num"]+blocks_num} {self.max_token_num} {self.cfg.max_seq_len}'

        # save block
        filename = self.dest_path / f'{self.rank}_{self.idx}'
        torch.save(examples, filename)
        self.idx += 1
        self.average_sent_num.append(len(examples['src_ids']) - 1 + len(examples['tgt_ids']))

        pre_tokens = blocks_num
        pushed_uttr_num = 0
        need_place = utterance_num if utterance_num is not None else len(self._doc_ids[0])
        remain_tokens = self.max_token_num - pre_tokens
        while (remain_tokens < need_place) or (self.uttr_num >= self.max_ctx_turn):
            tmp_pushed_uttr_num = self._push_block()
            pushed_uttr_num += tmp_pushed_uttr_num
            remain_tokens += tmp_pushed_uttr_num
            assert len(self._doc_ids) == len(self._doc_embeds[
                                                 'speaker_embedding']), f'{len(self._doc_ids)} != {len(self._doc_embeds["speaker_embedding"])}'
            assert len(self._doc_ids) == self.uttr_num, f'{len(self._doc_ids)} != {self.uttr_num}'

        return pushed_uttr_num
