# mlm_masking.py
# for masking scheme for UniLM pre-training
# jjl
# 2021-04-29
# ==========================

import json, random
import math
import numpy as np

class MaskingScheme:
    def __init__(self, cfg):
        self.cfg = cfg
        self.mask_ratio = self.cfg.mask_ratio if self.cfg.mask_ratio is not None else 0

    def mask(self, tokens):
        pass


class UniLMMaskingScheme(MaskingScheme):
    def __init__(self, cfg, tokenizer, pad_id, mask_id, sep_id, global_id):
        super().__init__(cfg)
        self.pad_id = pad_id
        self.tokens = tokenizer.s.get_piece_size()
        self.mask_id = mask_id
        self.sep_id = sep_id
        self.global_id = global_id

    def mask(self, token_ids):
        """

        :param token_ids:
        :return:
        """
        sent_length = len(token_ids)
        mask_num = math.ceil(sent_length * self.mask_ratio)
        _masks = [idx for idx, s in enumerate(token_ids[:sent_length]) if s != self.global_id]
        masks_indeces = np.random.choice(_masks[1:], mask_num, replace=False)
        masks_indeces.sort()
        return self.unilm_masking(token_ids, masks_indeces)

    def unilm_masking(self, token_ids, mask_idxes):
        def _random_gram_masking(token_ids, target_ids, cur_idx, last_nonpad_idx, use_mask_token=False):
            rand = np.random.random()
            if rand < 0.8:
                num_gram = 1
            else:
                num_gram = min(np.random.randint(2, 4), last_nonpad_idx - cur_idx + 1)
                if self.global_id in token_ids[cur_idx: cur_idx + num_gram]:
                    num_gram = np.where(token_ids[cur_idx: cur_idx + num_gram] == self.global_id)[0].item()

            if use_mask_token:
                tokens = [self.mask_id for _ in range(num_gram)]
            else:
                tokens = [np.random.randint(self.tokens) for _ in range(num_gram)]

            target_ids[cur_idx: cur_idx + num_gram] = pre_token_ids[cur_idx: cur_idx + num_gram].copy()
            token_ids[cur_idx: cur_idx + num_gram] = np.array(tokens)

        pre_token_ids = np.copy(token_ids)
        token_ids = np.copy(token_ids)
        nonpad_idx = np.where(token_ids == self.global_id)[0][-1].item()
        sent_len = len(token_ids)
        target = np.array([self.pad_id] * sent_len)

        for i in mask_idxes:
            if target[i - 1] == self.pad_id:
                rand = np.random.random()
                if rand < 0.8:
                    _random_gram_masking(token_ids, target, i, nonpad_idx, use_mask_token=True)
                elif rand < 0.9:
                    _random_gram_masking(token_ids, target, i, nonpad_idx, use_mask_token=False)

        assert len(token_ids) == len(target)
        return token_ids, target
