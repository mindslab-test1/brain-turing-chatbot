import torch
import random


class Mask:
    def mask(self, seq, glb_id, pad_id, line=None):
        """
        :param seq: 1-dim list or tensor
        :param glb_id: end of sequence token
        :return: mask tensor (bool type, not yet cuda)
        """

        if not isinstance(seq, torch.Tensor):
            seq = torch.tensor(seq)
        seq_len = seq.size(0)

        assert seq.dim() == 1, "dim() must be 1"

        glb_idx_list = [0] + ((seq == glb_id).nonzero(as_tuple=True)[0] + 1).tolist()
        pad_len = len((seq == pad_id).nonzero(as_tuple=True)[0])
        length = len(glb_idx_list) - 1
        out_mask = torch.zeros([0, seq_len])
        line = int(line) if line is not None else random.randint(0, length)
        for idx in range(length):
            row_len = glb_idx_list[idx + 1] - glb_idx_list[idx]
            now_mask = torch.zeros([row_len, 0])
            for ix in range(length):
                if idx == ix:
                    if idx < line:
                        mask = torch.ones([row_len, row_len])
                    else:
                        mask = torch.ones([row_len, row_len]).tril()
                else:
                    col_len = glb_idx_list[ix + 1] - glb_idx_list[ix]
                    if (idx < line and ix < line) or idx > ix:
                        mask = torch.ones([row_len, col_len])
                    else:
                        mask = torch.zeros([row_len, col_len])

                now_mask = torch.cat([now_mask, mask], dim=-1)

            row_pad_mask = torch.zeros([row_len, pad_len])
            now_mask = torch.cat([now_mask, row_pad_mask], dim=-1)
            out_mask = torch.cat([out_mask, now_mask], dim=0)
        col_pad_mask = torch.zeros([pad_len, seq_len])
        out_mask = torch.cat([out_mask, col_pad_mask], dim=0)
        return out_mask.bool()


if __name__ == "__main__":
    from pprint import pprint
    m = Mask()
    test = [0, 1, 2, 27, 15, 3, 4, 7, 0, 5, 6, 81, 0, 1, 5, 7, 8, 8, 8]
    out_mask = m.mask(seq=test, glb_id=7, pad_id=8, line=0)
    pprint(out_mask.tolist(), width=300)
    out_mask = m.mask(seq=test, glb_id=7, pad_id=8, line=len(test))
    pprint(out_mask.tolist(), width=300)
    out_mask = m.mask(seq=test, glb_id=7, pad_id=8, line=None)
    pprint(out_mask.tolist(), width=300)