# https://pytorch-lightning.readthedocs.io/en/stable/extensions/datamodules.html


import torch
from torch.utils.data import Dataset, ConcatDataset, DataLoader, random_split
import pandas as pd
from typing import Any, List, Mapping, Optional, Sequence, Tuple, Union

import pytorch_lightning as pl
from prefetch_generator import BackgroundGenerator, background
from vocab.MCSTokenizer import MCSTokenizer
from data.mlm_dataset import NoNSPUniLMDataset

class DataLoader_back(DataLoader):

    def __iter__(self):
        return BackgroundGenerator(super().__iter__())



class LitDataModule(pl.LightningDataModule):
    def __init__(self, cfg, tokenizer, data_dir: str = "path/to/dir", batch_size: int = 32):
        super().__init__()
        self.cfg = cfg
        self.tokenizer = tokenizer
        self.data_dir = data_dir
        self.batch_size = batch_size

        self.seed = self.cfg.seed

    def setup(self, stage=None):
        # build vocabulary
        # perform train/val/test split

        if stage == 'fit' or stage is None:
            # self.train_dataset, self.eval_dataset = get_dataset('fit')
            # TODO 깔끔하게;
            full_dataset = NoNSPUniLMDataset(self.cfg, 'train', self.tokenizer, self.seed)
            total_data_num = len(full_dataset)
            train_data_num = int(total_data_num * 0.9)
            dev_data_num = total_data_num - train_data_num
            self.train_dataset, self.eval_dataset = random_split(
                full_dataset,
                [train_data_num, dev_data_num],
                torch.Generator().manual_seed(self.seed)
            )  # random_split can recognize '-1'?

        if stage == 'test':
            self.test_dataset = NoNSPUniLMDataset(self.cfg, 'test', self.tokenizer, self.seed)

    def train_dataloader(self) -> Any:
        return DataLoader(self.train_dataset, batch_size=self.batch_size, num_workers=self.cfg.num_workers, pin_memory=True)

    def val_dataloader(self) -> Union[DataLoader, List[DataLoader]]:
        return DataLoader(self.eval_dataset, batch_size=self.batch_size, num_workers=self.cfg.num_workers, pin_memory=True)

    def test_dataloader(self) -> Union[DataLoader, List[DataLoader]]:
        return DataLoader(self.test_dataset, batch_size=self.batch_size, num_workers=self.cfg.num_workers, pin_memory=False)


import argparse

from vocab.Change import Change
from vocab.MCSTokenizer import MCSTokenizer
import sentencepiece as spm
from konlpy.tag import Mecab
import torch
from tqdm import tqdm
from pathlib import Path

# if __name__ == "__main__":
#     parser = argparse.ArgumentParser()
#     parser.add_argument('--mask_ratio', default='0.15', type=float)
#     parser.add_argument('--max_seq_len', default='2048', type=int)
#     parser.add_argument('--stride', default=6)
#     parser.add_argument('--c', default=1)
#     parser.add_argument('--r', default=1)
#     parser.add_argument('--w', default=2)
#     parser.add_argument('--seed', default=34)

#     cfg = parser.parse_args()
#     sp = spm.SentencePieceProcessor(model_file='/workspace/brain-turing-chatbot/vocab/mecab_change_50K_uni.model')
#     tokenizer = MCSTokenizer(tagger=Mecab(), change=Change(), sp=sp)
#     lit = LitDataLoader(cfg, tokenizer, data_dir="/data/engines/lm/unilm/corpus/mp_corpus/", batch_size=32) 

#     print('done with no error.')

#     print(list(result[0]))
#     print(list(result[1]))
#     for r1,r2 in zip(result[0][:20], result[1][:20]):
#         print(r1, r2)
