import json, glob, tqdm


def pp():
    data = {"title": f"AI Hub 감성 대화",
            "data": []}
    print(data['title'], end=' ')
    a = glob.glob('*.json')
    mem = 0
    for ii, i in enumerate(a):
        with open(i, 'r', encoding='utf-8') as f:
            b = json.load(f, strict=False)

        print(len(b), end=' ')
        co = 0
        for jj, j in tqdm.tqdm(enumerate(b)):
            datum = {"id": f"AHEM{jj+mem:08d}",
                     "dialog": []}

            for kk, k in enumerate(j['talk']['content'].items()):
                utter = {"id": f"{kk:03d}",
                         "speaker": "A" if k[0].startswith('H') else "B",
                         "utterance": k[1]}
                datum['dialog'].append(utter)
            co += len(datum['dialog'])
            data['data'].append(datum)
        mem = jj + 1
        print(co, co/len(b))

    with open(f"AHEM({len(data['data'])}).json", 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False)


def check():
    a = glob.glob('*0).json')
    for i in a:
        with open(i, 'r', encoding='utf-8') as f:
            b = json.load(f, strict=False)
        print(i, len(b), b[0]['title'])
        c = 0
        for j in b:
            c += len(j['dialog'])
        print(c)


if __name__ == "__main__":
    pp()



