import json, glob, tqdm


def pp():
    a = glob.glob('*.json')
    for ii, i in enumerate(a):
        with open(i, 'r', encoding='utf-8') as f:
            b = json.load(f, strict=False)
        data = {"title": f"AI Hub 한국어 SNS 일상대화 ({b['data'][0]['header']['dialogueInfo']['topic']})",
                "data": []}
        print(data['title'], len(b['data']), end=' ')
        co = 0
        for jj, j in tqdm.tqdm(enumerate(b['data'])):
            datum = {"id": f"AHS{chr(ii+65)}{jj:08d}",
                     "dialog": []}
            pid = {}
            for kk, k in enumerate(j['header']['participantsInfo']):
                pid[k['participantID']] = chr(kk+65)
            for kk, k in enumerate(j['body']):
                utter = {"id": f"{kk:03d}",
                         "speaker": pid[k['participantID']],
                         "utterance": k['utterance']}
                datum['dialog'].append(utter)
            co += len(datum['dialog'])
            data['data'].append(datum)
        print(co, co/len(b['data']))
        with open(f"AHS{chr(ii+65)}({len(b['data'])}).json", 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False)


def check():
    a = glob.glob('*0).json')
    for i in a:
        with open(i, 'r', encoding='utf-8') as f:
            b = json.load(f, strict=False)
        print(i, len(b), b[0]['title'])
        c = 0
        for j in b:
            c += len(j['dialog'])
        print(c)


if __name__ == "__main__":
    pp()



