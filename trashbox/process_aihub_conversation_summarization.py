import json, glob
from argparse import ArgumentParser
from dialogue_preprocessor import (
    FileProcessor, 
    DialoguePreprocessor,
    UtteranceProcessor,
    Stripper,
)

class DialogueSummaryDatasetFileProcessor(FileProcessor):
    def __init__(self, data_dir, raw_file_suffix):
        self.data_dir = data_dir
        self.raw_file_suffix = raw_file_suffix

    def extract_dialogues(self, file_path):
        with open(file_path, 'r') as f:
            file_dict = json.load(f)
        return file_dict['data']
    
    def extract_utterance_speaker_id_pairs(self, dialogue):
        utterance_objects = dialogue['body']['dialogue']
        utterance_speaker_id_pairs = [(dialogue_object['utterance'], dialogue_object['participantID']) for dialogue_object in utterance_objects]
        return utterance_speaker_id_pairs
    
    def load_files(self):
        files = glob.glob(self.data_dir + '/**/*' + self.raw_file_suffix, recursive=True)
        return files

def parse_args():
    parser = ArgumentParser()
    # parser = DialoguePreprocessor.add_argparse_args(parser)
    parser.add_argument('--code', default='AHCS')
    parser.add_argument('--destination_path', default='/root/engines/GT/aihub_korean_conversation_summarization/conv_summ.json')
    parser.add_argument('--title', default='AI Hub 한국어 대화 요약')
    parser.add_argument('--num_random_samples_to_print', type=int, default=10)
    parser.add_argument('--data_dir', default='/root/engines/aihub_korean_conversation_summarization')
    parser.add_argument('--raw_file_suffix', default='.json')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    utterance_processor = UtteranceProcessor(
        join_partial_utterances_callback=lambda partial_utterances: '\n'.join(partial_utterances),
        partial_utterance_processors=[Stripper()],
        complete_utterance_processors=[],
    )
    file_processor = DialogueSummaryDatasetFileProcessor(data_dir=args.data_dir, raw_file_suffix=args.raw_file_suffix)
    dialogue_preprocessor = DialoguePreprocessor.from_argparse_args(args, file_processor=file_processor, utterance_processor=utterance_processor)
    dialogue_preprocessor.execute()