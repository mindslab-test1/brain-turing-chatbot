import json
import re
from pathlib import Path
from tqdm import tqdm

SARW, SBRW, SDRW, SERW = [], [], [], []
# 폴더를 확인해서 각 파일 한개씩만 가져온다
files = {'SARW':[[], '국립국어원 구어말뭉치 공적독백', 0],
        'SBRW':[[], '국립국어원 구어말뭉치 공적대화', 0],
        'SDRW':[[], '국립국어원 구어말뭉치 사적대화', 0],
        'SERW':[[], '국립국어원 구어말뭉치 대본', 0]
        }

spoken_dir = Path('/data/engines/nikl_spoken')
#i = 0
for f in spoken_dir.glob('*'):
    for s in files.keys():
        if str(f.name).startswith(s): 
            files[s][0].append(f)
            files[s][2] += 1
            break
for k,v in files.items():
    v[0].sort()

print(f"A:{files['SARW'][2]}, B: {files['SBRW'][2]}, D: {files['SDRW'][2]}, E: {files['SERW'][2]}")



speaker_IDs = [chr(s) for s in range(ord('A'), ord('Z') +1)]
for s1 in list(range(ord('a'), ord('z')+1)):
    for s2 in list(range(ord('A'), ord('Z') +1)):
        speaker_IDs.append(f'{chr(s2)}{chr(s1)}')

for k,v in files.items():
    conversations = []
    not_dialogs_num = 0
    dialogs_num = 0
    utterances_num = 0
    filename = f'{k}.json'
    for f in tqdm(v[0], desc="load file..."):
        dialogs = []
        with open(f, 'r') as j_file:
            # 파일 당 conversation 하나씩 저장되어있다.
            # 파일을 열었을 때 speaker의 수를 파악한다
            # speaker를 A, B ...로 치환해야 한다.
            # 혹은 2인 화자로 제한한다.
            
            tmp_conversation = json.load(j_file)
        utterances = tmp_conversation['document'][0]['utterance']
       
        tmp_speakers = []
        for u in utterances:
            speakers = u['speaker_id'].split(',')
            for speaker in speakers:
                speaker = speaker.strip()
                if speaker not in tmp_speakers:
                    tmp_speakers.append(speaker) 
            #tmp_speakers.extend(speakers)

        if "" in tmp_speakers:
            tmp_speakers.remove("")

        #check speakers num
        speaker_DICT = dict()

        #speaker_IDs = [f"{s1}{s2}" for s1, s2 in zip(list(range(ord('A'), ord('Z') +1)), list(range(ord('a'), ord('z')+1)))]
        for speaker, speaker_id in zip(tmp_speakers, speaker_IDs):
            speaker_DICT[speaker] = speaker_id

        # 그 후 tmp_conversation['document'][0]['utterance']를 순회하며 utterance를 수집한다.
        # 이 때, 같은 화자의 발화는 하나로 묶어줘야 한다.
        # dialogs = []

        utterance_id = 0
        tmp_utterance = []
        tmp_speaker = ""
        for u in utterances:
            if u['speaker_id'].strip() == "" or u['form'] == "":
                continue

            trim_speaker = u['speaker_id'].strip().split(',')[0]
            trim_speaker = trim_speaker.strip()
            
            if not tmp_speaker or tmp_speaker == trim_speaker:
                tmp_speaker = trim_speaker
                tmp_utterance.append(u['form'])
            else:
                try:
                    dialogs.append({
                        "id": '{0:03d}'.format(utterance_id),
                        "speaker": speaker_DICT[tmp_speaker],
                        "utterance": ' '.join(tmp_utterance)
                        })
                    utterance_id += 1

                except KeyError:
                    print(tmp_speaker)
                    print(speaker_DICT)
                    
                tmp_utterance = [u['form']]
                tmp_speaker = trim_speaker
        if tmp_utterance:
            dialogs.append({
                "id": '{0:03d}'.format(utterance_id),
                "speaker": speaker_DICT[trim_speaker],
                "utterance": ' '.join(tmp_utterance)
                })
        
        if dialogs:
            conversations.append({
                'id':'{0}{1:08d}'.format(k, dialogs_num),
                'title':f"{v[1]}_{tmp_conversation['id']}",
                'dialog':dialogs
                })
            dialogs_num += 1
            utterances_num += utterance_id
        else:
            not_dialogs_num += 1
    
    save_file = Path('.',filename)
    with open(save_file, 'w', encoding='utf-8') as w_file:
        json.dump(conversations, w_file, indent=4, ensure_ascii=False)
    print(f'{k} file saved at {str(save_file)}')
    print(f'processed conversation: {dialogs_num} , passed conversation:{not_dialogs_num}')
    print(f'processed utterances: {utterances_num}')
    #print(f'total conversation:{files[k][-1]} == {dialogs_num + not_dialogs_num}')
    print('=' * 30)


