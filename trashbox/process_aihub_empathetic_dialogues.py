import json, glob, re
from argparse import ArgumentParser
from dialogue_preprocessor import (
    FileProcessor, 
    DialoguePreprocessor,
    UtteranceProcessor,
    Stripper,
)
from pathlib import Path

class DialogueSummaryDatasetFileProcessor(FileProcessor):
    def __init__(self, data_dir, raw_file_suffix):
        self.data_dir = data_dir
        self.raw_file_suffix = raw_file_suffix

    def extract_dialogues(self, file_path):
        with open(file_path, 'r') as f:
            file_dict = json.load(f)
        return file_dict
    
    def extract_utterance_speaker_id_pairs(self, dialogue):
        speaker_id_utterance_key_values = dialogue['talk']['content']
        utterance_speaker_id_pairs = [
            (re.search(r'[^\d]*', utterance).group(), re.search(r'[^\d]*', speaker_id).group()) 
            for speaker_id, utterance in speaker_id_utterance_key_values.items()
        ]
        return utterance_speaker_id_pairs
    
    def load_files(self):
        files = glob.glob(self.data_dir + '/**/*' + self.raw_file_suffix, recursive=True)
        files = [file_path for file_path in files if re.fullmatch(r'^감성대화말뭉치\(최종데이터\).*\.json$', Path(file_path).name)]
        return files

def parse_args():
    parser = ArgumentParser()
    # parser = DialoguePreprocessor.add_argparse_args(parser)
    parser.add_argument('--code', default='AHEM')
    parser.add_argument('--destination_path', default='/root/engines/GT/empathetic_dialogues.json')
    parser.add_argument('--title', default='aihub_empathetic_dialogues')
    parser.add_argument('--num_random_samples_to_print', type=int, default=10)
    parser.add_argument('--data_dir', default='/root/engines/aihub_empathetic_dialogues/')
    parser.add_argument('--raw_file_suffix', default='.json')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    utterance_processor = UtteranceProcessor(
        join_partial_utterances_callback=lambda partial_utterances: '\n'.join(partial_utterances),
        partial_utterance_processors=[Stripper()],
        complete_utterance_processors=[],
    )
    file_processor = DialogueSummaryDatasetFileProcessor(data_dir=args.data_dir, raw_file_suffix=args.raw_file_suffix)
    dialogue_preprocessor = DialoguePreprocessor.from_argparse_args(args, file_processor=file_processor, utterance_processor=utterance_processor)
    dialogue_preprocessor.execute()