import statistics
from typing import Sequence, List

class DialogueStatisticsCalculator:
    def __init__(self, dialogues):
        self.dialogues = dialogues

    # TODO: Make it possible to iterate the list of meaningful values.

    @property
    def total_num_dialogues(self) -> int:
        if getattr(self, '_total_num_dialogues', None) is None:
            self._total_num_dialogues = len(self.dialogues['data'])
        return self._total_num_dialogues

    @property
    def total_num_utterances(self) -> int:
        if getattr(self, '_total_num_utterances', None) is None:
            num_utterances = 0
            for dialogue in self.dialogues['data']:
                num_utterances += len(dialogue['dialog'])
            self._total_num_utterances = num_utterances
        return self._total_num_utterances

    @property
    def avg_utterances_per_dialogue(self) -> float :
        if getattr(self, '_avg_utterances_per_dialogue', None) is None:
            self._avg_atterances_per_dialogue = self.total_num_utterances / self.total_num_dialogues
        return self._avg_atterances_per_dialogue

    @property
    def list_of_dialogue_lengths(self) -> List[int]:
        if getattr(self, '_list_of_dialougue_lengths', None) is None:
            self._list_of_dialogue_lengths = [len(dialogue['dialog']) for dialogue in self.dialogues['data']]
        return self._list_of_dialogue_lengths

    @property
    def median_utterances_per_dialogue(self) -> float:
        if getattr(self, '_mean_utterances_per_dialogue', None) is None:
            self._mean_utterances_per_dialgue = statistics.median(self.list_of_dialogue_lengths)
        return self._mean_utterances_per_dialgue

    @property 
    def list_of_number_of_speakers(self) -> List[int]:
        if getattr(self, '_list_of_number_of_speakers', None) is None:
            self._list_of_number_of_speakers = [ len({utterance['speaker'] for utterance in dialogue['dialog']}) for dialogue in self.dialogues['data']]
        return self._list_of_number_of_speakers

    @property 
    def max_num_speakers(self) -> int:
        if getattr(self, '_max_num_speakers', None) is None:
            self._max_num_speakers = max(self.list_of_number_of_speakers)
        return self._max_num_speakers

    @property 
    def avg_num_speakers(self) -> float:
        if getattr(self, '_avg_num_speakers', None) is None:
            self._avg_num_speakers = sum(self.list_of_number_of_speakers) / len(self.list_of_number_of_speakers)
        return self._max_num_speakers
    
    @property
    def list_of_number_of_characters(self) -> List[int]:
        if getattr(self, '_list_of_number_of_characters', None) is None:
            self._list_of_number_of_characters = [len(utterance['utterance']) for dialogue in self.dialogues['data'] for utterance in dialogue['dialog']]
        return self._list_of_number_of_characters

    @property
    def max_num_characters_per_utterance(self) -> List[int]:
        if getattr(self, '_max_num_characters_per_utterance', None) is None:
            self._max_num_characters_per_utterance = max(self.list_of_number_of_characters)
        return self._max_num_characters_per_utterance

    @property
    def avg_num_characters_per_utterance(self) -> List[int]:
        if getattr(self, '_avg_num_characters_per_utterance', None) is None:
            self._avg_num_characters_per_utterance = sum(self.list_of_number_of_characters) / len(self.list_of_number_of_characters)
        return self._avg_num_characters_per_utterance