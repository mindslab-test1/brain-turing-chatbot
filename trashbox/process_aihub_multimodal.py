import json, pprint, glob, re, random, unicodedata
from json.decoder import JSONDecodeError
from pathlib import Path
from argparse import ArgumentParser
from tqdm import tqdm

def extract_dialogue(json_dict):
    dialogue = []
    speaker_ids = {}
    cur_speaker_id = 'A'
    cur_turn = 0
    prev_dialogue = None
    for utterance, speaker_id in [(dialogue['utterance'], dialogue['speaker_id'].strip()) for dialogue in json_dict['dialogue_infos']]:
        assert cur_turn < 1000, 'id excceded 999'
        assert speaker_id.lower() != 'none', 'The script contains an unidentifiable speaker'
            
        if speaker_id not in speaker_ids:
            assert ord('A') <= ord(cur_speaker_id) <= ord('Z'), 'speaker code exceeded Z'
            speaker_ids[speaker_id] = cur_speaker_id
            cur_speaker_id = chr(ord(cur_speaker_id) + 1)

        if prev_dialogue is None:
            prev_dialogue = {
                'id': str(cur_turn).zfill(3),
                'speaker': speaker_ids[speaker_id],
                'utterance': [' '.join(unicodedata.normalize('NFKC', utterance).split())],
            }
            continue

        if prev_dialogue['speaker'] != speaker_ids[speaker_id]:
            # Remoce \t and \xa0 in the utterance string and replace three consecutive dots symbol with "...".
            prev_dialogue['utterance'] = ' '.join(prev_dialogue['utterance'])
            dialogue.append(prev_dialogue)
            cur_turn += 1
            prev_dialogue = {
                'id': str(cur_turn).zfill(3),
                'speaker': speaker_ids[speaker_id],
                'utterance': [' '.join(unicodedata.normalize('NFKC', utterance).split())],
            }
        else:
            prev_dialogue['utterance'].append(' '.join(unicodedata.normalize('NFKC', utterance).split()))

    if prev_dialogue is not None:
        prev_dialogue['utterance'] = ' '.join(prev_dialogue['utterance'])
        dialogue.append(prev_dialogue)
    return dialogue

def process_json_file(json_file_path, object_id):
    with open(json_file_path, 'r') as f:
        file_dict = json.load(f)

    dialog = extract_dialogue(file_dict)
    output_dict = {
        'id': object_id,
        'title': Path(json_file_path).name,
        'dialog': dialog,
    } 
    return output_dict

def parse_args():
    parser = ArgumentParser()
    parser.add_argument('--code')
    parser.add_argument('--data_dir')
    parser.add_argument('--destination_path')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    dialogues = []
    files = glob.glob(args.data_dir+'/**/*.json', recursive=True)
    files = [ file_path for file_path in files if re.fullmatch(r'^.*KETI_MULTIMODAL_[0-9]{10}_interpolation.json$', file_path) ]
    files = sorted(files, key= lambda s: int(re.search(r'KETI_MULTIMODAL_(\d+)_interpolation.json', s).group(1))) 
    file_idx = 0
    for file_path in tqdm(files):
        assert file_idx <= 99999999, 'the number of dialogues exceeded 99999999'
        try:
            object_id = args.code + str(file_idx).zfill(8)
            dialogue = process_json_file(file_path, object_id=object_id)
            if len(dialogue['dialog']) < 2:
                print('len(dialog) is less than 2. This conversation will not be added to the final output.')
                print(file_path)
                print(dialogue['dialog'])
                continue
            dialogues.append(dialogue)
            file_idx += 1
        except (UnicodeDecodeError, JSONDecodeError) as e:
            print(e)
            print(f'Failed to decode {file_path}')
            pass
        except AssertionError as e:
            # print(e)
            # print(f'Assetion failed {file_path}')
            pass
        except Exception as e:
            print(e)
            print(file_path)
            raise e
    
    print('Print out some samples')
    samples = random.sample(dialogues, 3)
    for sample in samples:
        pprint.pprint(sample)
    
    dest_path = Path(args.destination_path)
    # What is wrong with with_stem()?
    dest_path = dest_path.with_name(f'{dest_path.stem}_({len(dialogues)}){dest_path.suffix}')
    with open(dest_path, 'w') as f:
        json.dump(dialogues, f, indent=4, sort_keys=True, ensure_ascii=False)
    print(f'Saved to {dest_path}') 