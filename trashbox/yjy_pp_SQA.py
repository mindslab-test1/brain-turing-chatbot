import json, glob, tqdm


def pp():
    a = glob.glob('*.json')
    for ii, i in enumerate(a):
        with open(i, 'r', encoding='utf-8') as f:
            b = json.load(f, strict=False)

        data = {"title": f"MINDs Lab 한국어 QA ({b[0]['title']})",
                "data": []}
        print(data['title'], len(b), end=' ')
        co = 0
        for jj, j in tqdm.tqdm(enumerate(b)):
            datum = {"id": f"SQA{chr(ii+65)}{jj:08d}",
                     "dialog": []}

            for kk, k in enumerate(j['dialog']):
                utter = {"id": f"{kk:03d}",
                         "speaker": k['speaker'],
                         "utterance": k['utterance']}
                datum['dialog'].append(utter)
            co += len(datum['dialog'])
            data['data'].append(datum)
        print(co, co/len(b))
        with open(f"SQA{chr(ii+65)}({len(b)}).json", 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False)


def check():
    a = glob.glob('*0).json')
    for i in a:
        with open(i, 'r', encoding='utf-8') as f:
            b = json.load(f, strict=False)
        print(i, len(b), b[0]['title'])
        c = 0
        for j in b:
            c += len(j['dialog'])
        print(c)


if __name__ == "__main__":
    pp()



