import json
import re
import argparse
from pathlib import Path
from tqdm import tqdm
import itertools

JSON_DICT = {'NIKL':['SARW','SBRW','SDRW','SERW'],
             'KETI':['KTDL', 'KTES','KTOL']}
TITLE_DICT = {'SARW': '국립국어원 구어말뭉치 공적독백',
              'SBRW': '국립국어원 구어말뭉치 공적대화',
              'SDRW': '국립국어원 구어말뭉치 사적대화',
              'SERW' : '국립국어원 구어말뭉치 대본',
              'KTDL': 'KETI 한국어 대화 데이터셋 일상대화',
              'KTES': 'KETI 한국어 대화 데이터셋 응급상황',
              'KTOL':'KETI 한국어 대화 데이터셋 오피스대화'}


speaker_IDs = [chr(s) for s in range(ord('A'), ord('Z') +1)]
for s1 in list(range(ord('a'), ord('z')+1)):
    for s2 in list(range(ord('A'), ord('Z') +1)):
        speaker_IDs.append(f'{chr(s2)}{chr(s1)}')

def stack_files_with_code_dict(codes, file_parent_dirs):
    code_num = len(codes)
    print(f'[Processing] process {code_num} codes. {codes}')
    files = {code:[[], 0] for code in codes}
    spoken_dir = Path(file_parent_dirs)

    for f in spoken_dir.glob('**/*'):
        if not f.is_dir():
            code = f.parents[0].stem
            if not code in list(itertools.chain(*JSON_DICT.values())):
                code = f.stem[:4]
            
            if (code in JSON_DICT['NIKL'] and f.suffix == '.json') or (code in JSON_DICT['KETI']):
                files[code][0].append(f)
                files[code][-1] += 1
    for k,v in files.items():
        v[0].sort()
        print(f'[ {k} ] total {files[k][-1]} files prepared')        
    return files

def process_utterance_format_json(code, tmp_conversation):
    if code == 'NIKL':
        utterances = tmp_conversation['document'][0]['utterance']
    elif code == "KETI":
        utterances = []
        tmp_uttr = []
        tmp_index = 0
        for utterance in tmp_conversation:
            if utterance['index'] != tmp_index:
                utterances.append(tmp_uttr)
                tmp_uttr = []
                tmp_index = utterance['index']
            if utterance['user_utterance'] != 'null':
                tmp_uttr.append(
                    {
                        'form': utterance['user_utterance'],
                        'speaker_id': 'A',
                    }
                )
            tmp_uttr.append(
                {
                    'form': utterance['system_utterance'],
                    'speaker_id': 'B',
                }
            )

    return utterances

def json_processing(code, corpus_file):
    utterances = 0
    tmp_conversation = json.load(corpus_file)
    for k,v in JSON_DICT.items():
        if code in v:
            utterances = process_utterance_format_json(k, tmp_conversation)
            break
    return utterances

def text_processing(corpus_file):
    utterances = []
    tmp_uttr = []
    while True:
        line = corpus_file.readline()
        if not line: break
        idx, utterance = line.split('\t')
        idx = int(idx)
        utterance = utterance.strip().replace("\ufeff", "")

        if idx == 1 and tmp_uttr:
            utterances.append(tmp_uttr)
            tmp_uttr = []
        tmp_uttr.append(
            {
                'form': utterance,
                'speaker_id': 'A' if int(str(idx)) % 2 == 1 else 'B',
            }
        )
    return utterances

def make_sample(utterances):
    # speakers
    tmp_speakers = []
    for u in utterances:
        speakers = u['speaker_id'].split(',')
        for speaker in speakers:
            speaker = speaker.strip()
            if speaker not in tmp_speakers:
                tmp_speakers.append(speaker)

    if "" in tmp_speakers:
        tmp_speakers.remove("")

    #check speakers num
    speaker_DICT = dict()
    for speaker, speaker_id in zip(tmp_speakers, speaker_IDs):
        speaker_DICT[speaker] = speaker_id

    dialog = []
    tmp_utterance = []
    utterance_id = 0
    tmp_speaker = ""
    
    for u in utterances:
        if u['speaker_id'].strip() == "" or u['form'] == "":
            continue

        trim_speaker = u['speaker_id'].strip().split(',')[0]
        trim_speaker = trim_speaker.strip()

        if not tmp_speaker or tmp_speaker == trim_speaker:
            tmp_speaker = trim_speaker
            tmp_utterance.append(u['form'])
        else:
            dialog.append({
                "id": '{0:03d}'.format(utterance_id),
                "speaker": speaker_DICT[tmp_speaker],
                "utterance": ' '.join(tmp_utterance)
                })
            utterance_id += 1

            tmp_utterance = [u['form']]
            tmp_speaker = trim_speaker
    if tmp_utterance:
        dialog.append({
            "id": '{0:03d}'.format(utterance_id),
            "speaker": speaker_DICT[trim_speaker],
            "utterance": ' '.join(tmp_utterance)
            })
    return dialog


def make_samples(corpus_files, destination_path):
    for k,v in corpus_files.items():
        conversations = []
        not_dialogs_num = 0
        dialogs_num = 0
        utterances_num = 0
        filename = f'{k}.json'
        dialogs = []
        for f in tqdm(v[0], desc=f"load {k}file..."):
            with open(f, 'r', encoding='utf-8-sig') as corpus_file:
                if f.suffix == '.json':
                    utterances = json_processing(k, corpus_file)
                elif f.suffix == '.txt':
                    utterances = text_processing(corpus_file)
                else:
                    raise Exception(f"we do not support file type : {f.suffix}")

            utterance_id = 0
            tmp_utterance = []
            if len(utterances) > 1 and isinstance(utterances[0], list):
                for utterance in utterances:
                    dialog = make_sample(utterance)
                    conversations.append({
                        'id': '{0}{1:08d}'.format(k, dialogs_num),
                        'dialog': dialog
                        })
                    dialogs_num += 1
                    utterances_num += len(dialog)

            else:
                dialog = make_sample(utterances)
                utterance_id += len(dialog)

                if dialog:
                    conversations.append({
                        'id':'{0}{1:08d}'.format(k, dialogs_num),
                        'dialog':dialog
                        })
                    dialogs_num += 1
                    utterances_num += utterance_id
                else:
                    not_dialogs_num += 1

        last_format = {
            'title': f"{TITLE_DICT[k]}",
            "data": conversations
        }

        save_file = Path(destination_path)
        save_file.mkdir(parents=True, exist_ok=True)
        save_file = save_file / filename
        with open(save_file, 'w', encoding='utf-8') as w_file:
            json.dump(last_format, w_file, indent=4, ensure_ascii=False)
        average_utterance_num = utterances_num / dialogs_num
        print(f'{k} file saved at {str(save_file)}')
        print(f'[result] processed conversation: {dialogs_num} , passed conversation:{not_dialogs_num}')
        print(f'[result] processed utterances: {utterances_num}')
        print(f'[result] average utterance num in conversation: {average_utterance_num} ')
        print('=' * 30)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--code', action='append')
    parser.add_argument('--data_dir')
    parser.add_argument('--destination_path')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    make_samples(stack_files_with_code_dict(args.code, args.data_dir), args.destination_path)







