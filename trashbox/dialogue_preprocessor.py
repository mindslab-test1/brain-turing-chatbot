import json, glob, re, random, unicodedata, os
from typing import Callable, Sequence
from json.decoder import JSONDecodeError
from pathlib import Path
from argparse import ArgumentParser, Namespace
from tqdm import tqdm
import itertools
from abc import ABC, abstractmethod
from typing import Any, Iterable, Dict, List
from pprint import pprint
from dialogue_statistics_calculator import DialogueStatisticsCalculator

# TODO: Add logging

class FileProcessor(ABC): 
    @abstractmethod
    def extract_dialogues(self, file_path: str) -> Iterable[Any]:
        pass

    @abstractmethod
    def extract_utterance_speaker_id_pairs(self, dialogue: Any) -> Iterable[Dict[str, str]]:
        pass

    @abstractmethod
    def load_files(self) -> Iterable[Any]:
        pass


# Just in case more complicated cases that require multiple attributes arise.
class UtteranceTransform(ABC):
    @abstractmethod
    def __call__(self, utterance: str):
        pass 

class NFKCNormalizer(UtteranceTransform):
    def __call__(self, utterance: str):
        return unicodedata.normalize('NFKC', utterance)

class WhitespaceNormalizer(UtteranceTransform):
    def __call__(self, utterance: str):
        return ' '.join(utterance.split())

# TODO: Is it double p? I am too afraid to Google it at the office.
class Stripper(UtteranceTransform):
    def __call__(self, utterance: str):
        return utterance.strip()


class UtteranceProcessor:
    def __init__(
        self, 
        join_partial_utterances_callback: Callable[[List[str]], str] = lambda x: x,
        partial_utterance_processors: Iterable[UtteranceTransform] = [],
        complete_utterance_processors: Iterable[UtteranceTransform] = [],
    ):
        self.partial_utterance_processors = partial_utterance_processors
        self.complete_utterance_processors = complete_utterance_processors
        self.join_partial_utterances_callback = join_partial_utterances_callback

    def process_partial_utterance(self, partial_utterance: str) -> Iterable[Any]:
        for partial_utterance_processor in self.partial_utterance_processors:
            partial_utterance = partial_utterance_processor(partial_utterance)
        return partial_utterance
    
    def process_complete_utterance(self, partial_utterance: str) -> Iterable[Any]:
        for complete_utterance_processor in self.complete_utterance_processors:
            partial_utterance = complete_utterance_processor(partial_utterance)
        return partial_utterance

    def join_partial_utterances(self, partial_utterances: List[str]) -> str:
        return self.join_partial_utterances_callback(partial_utterances)


class DialoguePreprocessor:
    def __init__(
        self, 
        code: str, 
        title: str, 
        destination_path: str, 
        file_processor: FileProcessor, 
        utterance_processor: UtteranceProcessor,
        num_random_samples_to_print: int = 3,
        **kwargs
    ):
        self.code = code
        self.title = title
        self.destination_path = destination_path

        self.file_processor = file_processor
        self.utterance_processor = utterance_processor
        self.num_random_samples_to_print = num_random_samples_to_print

    def execute(self):
        dialogues_list = []

        files = self.file_processor.load_files()
        print("Below are the list of files that will be processed.")
        pprint(files)
        print()

        print('Let us begin.')
        pbar = tqdm(files)
        for file_path in pbar:
            pbar.set_description(f'Processing {file_path}')
            try:
                dialogues = self._process_file(file_path)
                dialogues_list.append(dialogues)
            except Exception as e:
                print(e)
                print(f'An exception occured while processing f{file_path}.')
                continue

        # Flatten the list.
        dialogues = list(itertools.chain.from_iterable(dialogues_list)) 

        # Assign an id to each of the dialogues.
        for dialogue_idx, dialogue in enumerate(dialogues):
            # TODO: Get the maxinum from a config.
            assert dialogue_idx <= 99999999, 'the number of dialogues exceeded 99999999'
            # assert dialogue_idx <= self.config.max_dialogue_id, 'the number of dialogues exceeded 99999999'
            dialogue_id = self.code + str(dialogue_idx).zfill(8)
            dialogue['id'] = dialogue_id

        self.result = {
            'title': self.title,
            'data': dialogues,
        }

        self.print_samples()
        self.print_statistics() 
        self.save_dialogues()

    def _process_file(self, file_path: str):
        dialogues = []

        for utterance_objects in tqdm(self.file_processor.extract_dialogues(file_path), desc=file_path):

            utterance_speaker_id_pairs = self.file_processor.extract_utterance_speaker_id_pairs(utterance_objects)

            try:
                dialogue = self._extract_dialogue(utterance_speaker_id_pairs) 
            except AssertionError as e:
                print(e)
                print(f'Assertion failed {file_path}')
                continue
            except Exception as e:
                print(e)
                print(f'An exception occured while processing f{file_path}.')
                continue

            output_dict = {
                'dialog': dialogue,
            } 

            if len(output_dict['dialog']) < 2:
                print('len(dialog) is less than 2. This conversation will not be added to the final output.')
                print(file_path)
                pprint(output_dict['dialog'])
                continue

            dialogues.append(output_dict)

        return dialogues
    
    def print_samples(self):
        if self.num_random_samples_to_print > 0:
            print('Here are some random samples, do they seem alright?')
            samples = random.sample(self.result['data'], min(len(self.result['data']), self.num_random_samples_to_print))
            for sample in samples:
                pprint(sample)

    def save_dialogues(self):
        dest_path = Path(self.destination_path)

        # Create the parent directories if they do not already exist.
        dest_path.parent.mkdir(parents=True, exist_ok=True)

        # TODO: Delegate renaming to FileProcessor to make it optional.
        # What is wrong with with_stem()?
        dest_path = dest_path.with_name(f'{dest_path.stem}_({len(self.result["data"])}){dest_path.suffix}')

        print(f'Writing the contents to {dest_path}...') 
        with open(dest_path, 'w') as f:
            json.dump(self.result, f, indent=4, sort_keys=True, ensure_ascii=False)
        print(f'Saved to {dest_path}') 

    def _extract_dialogue(self, utterance_speaker_id_pairs: Iterable[Dict[str, str]]):
        dialogue = []
        speaker_ids = {}
        cur_speaker_id = 'A'
        cur_turn = 0
        prev_utterance = None

        for utterance, speaker_id in utterance_speaker_id_pairs:
            # TODO: Add validation check callback for the pair.

            # TODO: Get the number from a config.
            assert cur_turn < 1000, 'id excceded 999'

            # Register the current speaker ID if it is the first time seeing this speaker. 
            if speaker_id not in speaker_ids:
                assert ord('A') <= ord(cur_speaker_id) <= ord('Z'), 'speaker code exceeded Z'
                speaker_ids[speaker_id] = cur_speaker_id
                cur_speaker_id = chr(ord(cur_speaker_id) + 1)

            # Save the first utterance of the dialogue.
            if prev_utterance is None:
                prev_utterance = {
                    # TODO: Get the number from a config.
                    'id': str(cur_turn).zfill(3),
                    'speaker': speaker_ids[speaker_id],
                    'utterance': [utterance]
                    # 'utterance': [' '.join(unicodedata.normalize('NFKC', utterance).split())],
                }
                continue
            
            # Process accumulated utterances if there has been a turn change.
            # And set the current utterance as the previous utterance.
            if prev_utterance['speaker'] != speaker_ids[speaker_id]:
                # The below comment is hallucinating the past.
                # Remove \t and \xa0 in the utterance string and replace three consecutive dots symbol with "...".
                prev_utterance['utterance'] = [
                    self.utterance_processor.process_partial_utterance(partial_utterance) 
                    for partial_utterance in prev_utterance['utterance']
                ]
                prev_utterance['utterance'] = self.utterance_processor.join_partial_utterances(prev_utterance['utterance'])
                prev_utterance['utterance'] = self.utterance_processor.process_complete_utterance(prev_utterance['utterance'])

                # Add to the dialogue list if the accumulated utterance is not just whitespaces.
                if not prev_utterance['utterance'].isspace() and len(prev_utterance['utterance']) > 0:
                    dialogue.append(prev_utterance)
                    cur_turn += 1

                prev_utterance = {
                    # TODO: Get the number from a config.
                    'id': str(cur_turn).zfill(3),
                    'speaker': speaker_ids[speaker_id],
                    'utterance': [utterance],
                }
            # If it was the same speaker as the previous one, append the utterance at the end of the atterances list.
            else:
                prev_utterance['utterance'].append(utterance)

        # Check if there are any leftovers.
        if prev_utterance is not None:
            prev_utterance['utterance'] = [
                self.utterance_processor.process_partial_utterance(partial_utterance) 
                for partial_utterance in prev_utterance['utterance']
            ]
            prev_utterance['utterance'] = self.utterance_processor.join_partial_utterances(prev_utterance['utterance'])
            prev_utterance['utterance'] = self.utterance_processor.process_complete_utterance(prev_utterance['utterance'])
            if not prev_utterance['utterance'].isspace() and len(prev_utterance['utterance']) > 0:
                dialogue.append(prev_utterance)

        return dialogue
    
    def print_statistics(self):
        statistics_calculator = DialogueStatisticsCalculator(self.result)
        print(f'Total number of dialogues: {statistics_calculator.total_num_dialogues:,}')
        print(f'Total number of utterances: {statistics_calculator.total_num_utterances:,}')
        print(f'Average number of utterances per dialogue: {statistics_calculator.avg_utterances_per_dialogue:.2f}')
        print(f'Median of the number of utterances in a dialogue: {statistics_calculator.median_utterances_per_dialogue:.2f}')
        print(f'Maximum number of speakers: {statistics_calculator.max_num_speakers:,}')
        print(f'Average number of speakers: {statistics_calculator.avg_num_speakers:.2f}')
        print(f'Maximum number of characters in an utterance: {statistics_calculator.max_num_characters_per_utterance:,}')
        print(f'Average number of characters per utterance: {statistics_calculator.avg_num_characters_per_utterance:.2f}')


    @staticmethod 
    def add_argparse_args(parent_parser: ArgumentParser):
        parser = parent_parser.add_argument_group('DialogueProcessor arguments.')
        parser.add_argument('--code')
        parser.add_argument('--destination_path')
        return parent_parser

    @classmethod 
    def from_argparse_args(cls, args: Namespace, file_processor: FileProcessor, utterance_processor: UtteranceProcessor):
        # I Know...
        args_dict = vars(args)
        return cls(**args_dict, file_processor=file_processor, utterance_processor=utterance_processor) 